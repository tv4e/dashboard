'use strict';

//Request is designed to be the simplest way possible to make http calls.
// It supports HTTPS and follows redirects by default.
import axios from 'axios';
//cheerio is jQuery lib
import cheerio from 'cheerio';
import _ from 'lodash';
import { handleURL, generalEscape } from './HandleText';

/**
* WebScrapper
* Crawler receives bunch of data to handle. If has another links it will receive links and open one by one from inside a container each data has url, newsContainer, newsLink, contentContainer and content
*/
export default class WebScrapper {
  constructor(callback) {
    this._pagesToVisit = [];
    this._pageContent = [];
    this.callback = callback;
    this._exceptionTags = ['script', 'button', 'video', 'img'];
    this._exceptionDB = [];
  }

  crawl(data) {
    this._exceptionDB =
      data.html_exception != undefined ? JSON.parse(data.html_exception) : [];

    // Make the request
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/sources/url?url=${data.url}`
    })
      .then(response => {
        // Check status code (200 is HTTP OK)
        if (response.status !== 200) {
          return null;
        }

        // Parse the document body
        let $ = cheerio.load(response.data);

        if (data['news_link']) {
          this._pagesToVisit = this.collectLinks($, data);
        } else {
          let content = this.collectContent($, data);
          if (!_.isEmpty(content)) this._pageContent.push(content);
        }

        if (this._pagesToVisit.length == 0) {
          if (!_.isEmpty(this._pageContent)) {
            // infoLog(`Source: ${data.url} | DATA: ${this._pageContent}`);
            this.callback(null, this._pageContent);
          } else {
            this.callback('EmptyContent', null);
          }
        }

        console.log(data);

        if (this._pagesToVisit.length != 0) {
          this.crawl({
            url: handleURL(data.url, this._pagesToVisit[0]),
            content_container: data['content_container'],
            content_title: data['content_title'],
            content_description: data['content_description'],
            html_exception: data.html_exception
          });
          this._pagesToVisit.shift();
        }
      })
      .catch(error => {
        this.callback('WebScrapper: error', null);
        console.log('WebScrapper: error');
        console.log(error);
      });
  }

  singleCrawl(data) {
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/sources/url?url=${data.url}`
    })
      .then(response => {
        // Check status code (200 is HTTP OK)
        // console.log("Visiting page " + data.url);

        if (response.status !== 200) {
          return null;
        }

        // Parse the document body
        let $ = cheerio.load(response.data);

        let content = this.collectContent($, data);
        if (!_.isEmpty(content)) this.callback(null, content);
      })
      .catch(error => {
        console.log('WebScrapper: error');
        console.log(error);
      });
  }

  collectLinks($, data) {
    let newsContainer = data['news_container'];
    let newsLink = data['news_link'];
    let pagesToVisit = [];

    //for each element on the container create an index of objects
    $(newsContainer)
      .find(newsLink)
      .map(function(k, v) {
        let pagesToVisitObj = $(v).attr('href');

        if (_.indexOf(pagesToVisit, pagesToVisitObj) == -1) {
          pagesToVisit.push(pagesToVisitObj);
        }
      });

    if (_.isEmpty(pagesToVisit)) {
      // errorLog(`WebScrapper page has no Links: ${data.url}`);
    }

    return pagesToVisit;
  }

  collectContent($, data) {
    //Container is the element tag which holds all the new
    //Content is an object to search for all given content
    let contentContainer = data['content_container'];
    let content = {
      title: data['content_title'],
      description: data['content_description']
    };
    let hasException = false;
    let exceptionTags = this._exceptionTags;
    let exceptionDB = this._exceptionDB;

    if (!content.title || !content.description) {
      this.callback('No description or title values defined', null);
      console.log('No description or title values defined');
      return null;
    }

    let contentObject = {};

    // console.log('Generating content from link');
    let imageIndex = 1;

    //for each element on the container create an index of objects
    for (let i in content) {
      if (i == 'image') {
        let image = $(contentContainer)
          .find(content[i])
          .attr('src');
        contentObject[i] = image;
        imageIndex++;
      } else {
        contentObject[i] = '';

        $(contentContainer)
          .find(content[i])
          .map(function(k, v) {
            hasException = false;

            _.forEach(exceptionTags, exception => {
              $(contentContainer)
                .find(exception)
                .map(function(k2, v2) {
                  $(v2).remove();
                });
            });

            _.forEach(exceptionDB, exception => {
              $(contentContainer)
                .find(exception)
                .map(function(k2, v2) {
                  $(v2).remove();
                });
            });
            contentObject[i] += ' ' + generalEscape($(v).text());
          });
        contentObject[i] = contentObject[i].trim();
      }
    }

    if (
      !_.isEmpty(contentObject.title) &&
      !_.isEmpty(contentObject.description)
    ) {
      return contentObject;
    } else if (
      _.isEmpty(contentObject.title) ||
      _.isEmpty(contentObject.description)
    ) {
      if (_.isEmpty(contentObject.title)) {
        // errorLog(`No title set: ${data.url}`);
      }

      if (_.isEmpty(contentObject.description)) {
        // errorLog(`No description set: ${data.url}`);
      }

      this.callback('No description or title values found', null);

      return null;
    }
  }
}

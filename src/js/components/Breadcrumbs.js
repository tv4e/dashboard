import React from 'react';
import { Icon } from 'react-foundation';
import _ from 'lodash';

/**
* Breadcrumbs
* Used to locate the current page based on the route
*/
export default class Breadcrumbs extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { location } = this.props;
    let breadCrumb = '';

  /**
  * Gets the breadcrumb by splitting the router for instance /page/page2 = page > page2
  */
    _.split(location.pathname, '/').map(v => {
      if (!_.isEmpty(v)) {
        breadCrumb += ` > ${_.startCase(v)}`;
      }
    });

    return (
      <section className="breadcrumbs">
        <Icon name="fi-home" />
        {breadCrumb}
      </section>
    );
  }
}

Breadcrumbs.propTypes = {};

Breadcrumbs.defaultProps = {};

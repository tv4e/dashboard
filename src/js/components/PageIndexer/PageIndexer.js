import React from 'react';
import {
  Button,
  Inline,
  PaginationItem,
  PaginationPrevious,
  PaginationNext,
  PaginationEllipsis,
  Pagination
} from 'react-foundation';
import _ from 'lodash';

/**
* Page Indexer
* Indexes pages according to the number of elements of a list from a json object
*/
export default class PageIndexer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 1
    };
  }

  render() {
    let { pageNumber, setPageIndex } = this.props;
    let active = _.cloneDeep(this.state.active);

  /**
  * Uses a react-foundation pagination component
  * Pagination previous retrocedes one page 
  * Pages are get from function _pages
  * Pagination next advances one page  
  * If page 1 then previous is disabled. If last page then next is disabled
  */
    return (
      <div className="pageIndexer">
        <div className="pagination-center-example">
          <Pagination aria-label="Pagination" isCentered>
            {active == 1 ? (
              <PaginationPrevious isDisabled>
                Previous <Inline showForSr>page</Inline>
              </PaginationPrevious>
            ) : (
              <PaginationPrevious
                onClick={() =>
                  this.setState({ active: active - 1 }, () => {
                    let _active = _.cloneDeep(this.state.active);
                    setPageIndex(_active - 1);
                  })}
              >
                <a aria-label="Previous page">
                  Previous <Inline showForSr>page</Inline>
                </a>
              </PaginationPrevious>
            )}

            {this._pages()}

            {active == pageNumber ? (
              <PaginationNext isDisabled>
                Next <Inline showForSr>page</Inline>
              </PaginationNext>
            ) : (
              <PaginationNext
                onClick={() => {
                  this.setState({ active: active + 1 }, () => {
                    let _active = _.cloneDeep(this.state.active);
                    setPageIndex(_active - 1);
                  });
                }}
              >
                <a aria-label="Next page">
                  Next <Inline showForSr>page</Inline>
                </a>
              </PaginationNext>
            )}
          </Pagination>
        </div>
      </div>
    );
  }

  /**
  * Divides the pages according to the pageNumber prop. Sets the PaginationItems (numbers)
  */
  _pages = () => {
    let { pageNumber, setPageIndex } = this.props;
    let { active } = this.state;

    let pages = [];
    for (let i = active; i <= pageNumber; i++) {
      if (i == this.state.active) {
        pages.push(
          <PaginationItem key={`pageIndex_${i}`} isCurrent>
            <Inline showForSr>You're on page</Inline> {i}
          </PaginationItem>
        );
      } else {
        pages.push(
          <PaginationItem
            key={`pageIndex_${i}`}
            onClick={() =>
              this.setState({ active: i }, () => {
                let active = _.cloneDeep(this.state.active);
                setPageIndex(active - 1);
              })}
          >
            <a aria-label={`Page ${i}`}>{i}</a>
          </PaginationItem>
        );
      }

      if (i == active + 4) {
        if (pageNumber > active + 4) {
          pages.push(<PaginationEllipsis key={`pageEllipsis_0`} />);

          if (pageNumber > active + 5) {
            pages.push(
              <PaginationItem
                key={`pageIndex_${pageNumber - 1}`}
                onClick={() =>
                  this.setState({ active: pageNumber - 1 }, () => {
                    let active = _.cloneDeep(this.state.active);
                    setPageIndex(active - 1);
                  })}
              >
                <a aria-label={`Page ${pageNumber - 1}`}>{pageNumber - 1}</a>
              </PaginationItem>
            );
          }
          pages.push(
            <PaginationItem
              key={`pageIndex_${pageNumber}`}
              onClick={() =>
                this.setState({ active: pageNumber }, () => {
                  let active = _.cloneDeep(this.state.active);
                  setPageIndex(active - 1);
                })}
            >
              <a aria-label={`Page ${pageNumber}`}>{pageNumber}</a>
            </PaginationItem>
          );
        }

        break;
      }
    }

    return pages;
  };
}

PageIndexer.propTypes = {
  /**
  * Number of pages to divide the content to
  */
  pageNumber: React.PropTypes.number.isRequired
  // valueSelected: React.PropTypes.oneOfType([
  //   React.PropTypes.string,
  //   React.PropTypes.integer
  // ]),
};

PageIndexer.defaultProps = {};

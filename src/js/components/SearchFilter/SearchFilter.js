import React from 'react';
import { Icon } from 'react-foundation';
import DropdownList from 'react-widgets/lib/DropdownList';

/**
* Search Filter
* A text based search to filter content according to a text query
*/
export default class SearchFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null
    };
  }


/**
* Uses a Dropdownlist component from react-widgets to show and filter the content
*/
  render() {
    let { value } = this.state;
    let {
      filter,
      data,
      placeholder,
      defaultValue,
      groupBy,
      groupComponent,
      valueField,
      textField,
      onSelect,
      onChange
    } = this.props;

    let ValueInput = ({ item }) => {
      if (typeof item == 'string') {
        return <span>{item}</span>;
      } else {
        let separator =
          item.separator != undefined ? `${item.separator} | ` : '';

        return (
          <span>
            <strong>{separator}</strong>
            {item.value}
          </span>
        );
      }
    };

    let ListItem = ({ item }) => {
      if (typeof item == 'string') {
        return <span>{item}</span>;
      } else {
        return (
          <span>
            <strong>{item.prefix}</strong>
            {' ' + item.value}
          </span>
        );
      }
    };

    return (
      <div className="searchFilter">
        <div className="searchFilter__icon">
          <Icon name="fi-magnifying-glass" />
        </div>

        <DropdownList
          filter={filter}
          allowCreate="onFilter"
          data={data}
          placeholder={placeholder}
          defaultValue={defaultValue}
          valueComponent={ValueInput}
          itemComponent={ListItem}
          value={value}
          groupBy={groupBy}
          valueField={valueField}
          textField={textField}
          onChange={value => {
            this.setState({ value });
            if (onChange != undefined) {
              onChange(value);
            }
          }}
          onSelect={onSelect}
        />
        {value && (
          <div className="searchFilter__delete">
            <Icon
              onClick={() => {
                this.setState({ value: null });
                if (onChange != undefined) {
                  onChange(null);
                }
                if (onSelect != undefined) {
                  onSelect(null);
                }
              }}
              name="fi-x"
            />
          </div>
        )}
      </div>
    );
  }
}

SearchFilter.propTypes = {};

SearchFilter.defaultProps = {
  filter: 'startsWith'
};

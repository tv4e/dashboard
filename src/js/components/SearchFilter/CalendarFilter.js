import React from 'react';
import { Icon, Row, Column } from 'react-foundation';

import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';

/**
* Calendar Filter
* A calendar to filter content according to a date range
*/
export default class CalendarFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openFrom: false,
      openFrom: false,
      valueFrom: null,
      valueTo: null
    };

    /**
    * Moment Localizer sets calendar according to region
    */
    momentLocalizer();
  }

  /**
  * Uses a DateTimePicker componenet from react-widgets as an interface to select date
  * Needs to calendar to select the From value and the To value
  */
  render() {
    let { openFrom, openTo, valueFrom, valueTo } = this.state;
    let { onChange } = this.props;
    let {} = this.props;

    return (
      <div className="calendarFilter">
        <input
          className="blindInput"
          type="text"
          ref={input => {
            this.blindInput = input;
          }}
        />

        <Row>
          <Column>
            <div className="calendarFilter__DatePicker">
              <div className="calendarFilter__icon">
                <Icon name="fi-calendar" />
              </div>
              <DateTimePicker
                format={Moment(valueFrom).format('DD/MM/YYYY')}
                open={openFrom}
                onFocus={() => this.setState({ openFrom: 'date' })}
                onBlur={() => this.setState({ openFrom: false })}
                value={valueFrom}
                onToggle={() => {}}
                onChange={value => {
                  if (onChange != undefined) {
                    onChange({ from: value, to: valueTo });
                  }
                  this.setState({ openFrom: false, valueFrom: value }, () => {
                    this.blindInput.focus();
                  });
                }}
                placeholder="From"
                time={false}
              />
              {valueFrom && (
                <div className="calendarFilter__delete">
                  <Icon
                    onClick={() => {
                      this.setState({ valueFrom: null });

                      if (onChange != undefined) {
                        onChange({ from: null, to: valueTo });
                      }
                    }}
                    name="fi-x"
                  />
                </div>
              )}
            </div>
          </Column>

          <Column>
            <div className="calendarFilter__DatePicker">
              <div className="calendarFilter__icon">
                <Icon name="fi-calendar" />
              </div>
              <DateTimePicker
                format={Moment(valueTo).format('DD/MM/YYYY')}
                open={openTo}
                onFocus={() => this.setState({ openTo: 'date' })}
                onBlur={() => this.setState({ openTo: false })}
                value={valueTo}
                onToggle={() => {}}
                onChange={value => {
                  if (onChange != undefined) {
                    onChange({ to: value, from: valueFrom });
                  }
                  this.setState({ openTo: false, valueTo: value }, () => {
                    this.blindInput.focus();
                  });
                }}
                placeholder="To"
                time={false}
              />
              {valueTo && (
                <div className="calendarFilter__delete">
                  <Icon
                    onClick={() => {
                      this.setState({ valueTo: null });
                      if (onChange != undefined) {
                        onChange({ to: null, from: valueFrom });
                      }
                    }}
                    name="fi-x"
                  />
                </div>
              )}
            </div>
          </Column>
        </Row>
      </div>
    );
  }
}

CalendarFilter.propTypes = {};

CalendarFilter.defaultProps = {
  filter: 'startsWith'
};

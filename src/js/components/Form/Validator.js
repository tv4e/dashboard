import React from 'react';
import DropdownList from 'react-widgets/lib/DropdownList';
import validator from 'validator';

/**
* Form Validation - Max Char Length of input 
*/
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

/**
* Form Validation - Ḿin Char Length of input 
*/
const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;

/**
* Form Validation - Min integer value
*/
export const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;

/**
* Form Validation - Max integer value
*/
export const maxValue = max => value =>
  value && value > max ? `Can't be more than ${max}` : undefined;

/**
* Form Validation - Form Field is required
*/
export const required = value => (value ? undefined : 'Required');

/**
* Form Validation - Max Char Length of input 15 
*/
export const maxLength15 = maxLength(15);
/**
* Form Validation - Min Char Length of input 2 
*/
export const minLength2 = minLength(2);

/**
* Form Validation - Must be a number
*/
export const number = value =>
  value && isNaN(Number(value)) ? 'Must be a number' : undefined;

/**
* Form Validation - Must be an email
*/
export const email = value =>
  value && !validator.isEmail(value) ? 'Invalid email address' : undefined;

/**
* Form Validation - Must be an alphanumeric
*/
export const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined;

/**
* Form Validation - Must be a phone number
*/
export const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? 'Invalid phone number, must be 10 digits'
    : undefined;

/**
* Form Validation - Must be an URL
*/
export const url = value =>
  value && !validator.isURL(value, { allow_trailing_dot: true })
    ? 'Invalid URL'
    : undefined;


/**
* Render Field 
* Input Form Field with Error and Warnings Outputs according to validations
*/
export const RenderField = ({
  input,
  label,
  placeholder,
  type,
  meta: { touched, error, warning }
}) => {
  return (
    <div>
      <label>
        {label}
        <span className="input__error-text">
          {touched && ((error && '*') || (warning && '*'))}
        </span>
      </label>
      <div>
        <input
          {...input}
          className={
            touched &&
            ((error && 'input__error') || (warning && 'input__error'))
          }
          placeholder={placeholder}
          type={type}
        />
        {touched &&
          ((error && <div className="input__error-message">{error}</div>) ||
            (warning && <div className="input__error-message">{warning}</div>))}
      </div>
    </div>
  );
};

/**
* Render Dropdown List 
* Dropdown Form Field with Error and Warnings Outputs according to validations
*/
export const RenderDropdownList = ({
  input,
  data,
  label,
  placeholder,
  groupComponent,
  groupBy,
  valueField,
  textField,
  valueComponent,
  meta: { touched, error, warning }
}) => (
  <div className="dropdown">
    <label>
      {label}
      <span className="dropdown__error-text">
        {touched && ((error && '*') || (warning && '*'))}
      </span>
    </label>

    <div
      className={
        touched &&
        ((error && 'dropdown__error') || (warning && 'dropdown__error'))
      }
    >
      <DropdownList
        {...input}
        filter
        placeholder={placeholder}
        data={data}
        valueComponent={valueComponent}
        groupBy={groupBy}
        groupComponent={groupComponent}
        valueField={valueField}
        textField={textField}
        onChange={input.onChange}
      />
      {touched &&
        ((error && <div className="dropdown__error-message">{error}</div>) ||
          (warning && (
            <div className="dropdown__error-message">{warning}</div>
          )))}
    </div>
  </div>
);

import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column, Icon } from 'react-foundation';
import NormalSourceEdit from './NormalSourceEdit';
import ExpandSourceEdit from './ExpandSourceEdit';

/**
* Source edition - Parent component
* Connects NormalSourceEdit.js and ExpandSourceEdit.js to editSources and subsources
*/
export default class SourceEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showSubs: false,
      subData: null
    };
  }

  componentWillMount() {
    this.props.actionCreators.sources();
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { actionData, history } = this.props;
    let { showSubs, subData } = this.state;

    return (
      <section className="sourceEdit">
        <Row>
          <Column>
            {showSubs ? (
              <ExpandSourceEdit
                onHideSubs={() => this._onHideSubs()}
                subData={subData}
                history={history}
              />
            ) : (
              <NormalSourceEdit
                onShowSubs={data => this._onShowSubs(data)}
                actionData={actionData}
                history={history}
              />
            )}
          </Column>
        </Row>
      </section>
    );
  }

  /**
  * Shows sub sources page
  */
  _onShowSubs = data => {
    this.setState({ showSubs: true, subData: data });
  };

  /**
  * Hides subSources Page
  */
  _onHideSubs = data => {
    this.setState({ showSubs: false, subData: null });
  };
}

SourceEdit.propTypes = {};

SourceEdit.defaultProps = {};

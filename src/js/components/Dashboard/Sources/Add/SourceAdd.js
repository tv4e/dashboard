import React from 'react';
import { Row, Column, Button } from 'react-foundation';

import FormSourceAdd from './Form/FormSourceAdd';
import FormNewsContainer from './Form/FormSourceAddNews';
import FormNewsContent from './Form/FormSourceAddLinks';

import WebScrapper from '../../../Tools/WebScrapper';
import { connect } from 'react-redux';
import _ from 'lodash';
import { reduxForm, FormSection } from 'redux-form';

/**
* Source Add
* Component to add sources. Needs to meet certain requirements. News Source is required. ASGIE is required. City limits to whom the videos from the source will be sent. Depends in 3 different interconnected forms.
*/
class SourceAdd extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      crawling: null,
      crawlingValid: false,
      checkedCrawling: false
    };
  }

  /**
  * Before mounting component makes a HTTP request to get the ASGIE and cities
  */
  componentWillMount() {
    this.props.actionCreators.asgie();
    this.props.actionCreators.districts();
  }

  /**
  * Updates Props based on redux actionData
  */
  componentWillReceiveProps(nextProps) {
    if (
      !_.isEqual(
        this.props.actionData.postSources.data,
        nextProps.actionData.postSources.data
      )
    ) {
      if (nextProps.actionData.postSources.data.id != undefined) {
        alert('Success');
      } else {
        alert(nextProps.actionData.postSources.data);
      }
    }

    this.props = nextProps;
  }

  render() {
    let asgie = this.props.actionData.asgie.data;
    let districts = this.props.actionData.districts.data;
    let { crawler, checkedCrawling } = this.state;
    let { handleSubmit, submitting } = this.props;


    /**
    * The FormSection component interconnect the 3 forms
    */
    return (
      <section className="sourceAdd">
        <form onSubmit={handleSubmit(this._submit)}>
          <Row>
            <Column large={4}>
              <div className="title">Add a new source</div>
              <div>
                Start by adding a news source, defining the associated ASGIE and
                associating a location.
              </div>
            </Column>
            <Column large={8}>
              <FormSection name="sourcesForm">
                <FormSourceAdd
                  onChange={() => this.setState({ checkedCrawling: false })}
                  asgie={asgie}
                  districts={districts}
                />
              </FormSection>
            </Column>
          </Row>

          <hr />

          <Row>
            <Column large={4}>
              <div className="title">Locate the news list</div>
              <div>
                Enter the web site container where the news are listed and the
                news link.
              </div>
            </Column>
            <Column large={8}>
              <FormSection name="sourcesForm">
                <FormNewsContainer
                  onChange={() => this.setState({ checkedCrawling: false })}
                />
              </FormSection>
            </Column>
          </Row>

          <hr />

          <Row>
            <Column large={4}>
              <div className="title">Define where are the news</div>
              <div>
                Enter the container for the news and then locate the title and
                body of the news.<br />If necessary add html exceptions that the
                crawler should ignore
              </div>
            </Column>
            <Column large={8}>
              <FormSection name="sourcesForm">
                <FormNewsContent
                  onChange={() => this.setState({ checkedCrawling: false })}
                />
              </FormSection>
            </Column>
          </Row>

          <hr />

          <Row>
            <Column large={4}>
              <div className="title">Verify the crawling process</div>
              <div>
                In this area will be shown the results of the crawling process
              </div>
            </Column>
            <Column large={8}>
              <label>WebCrawler Results</label>
              <div className="sourceAdd__crawlerResults">
                {crawler == null
                  ? 'Crawling results will be shown here as a JSON object'
                  : crawler}
              </div>
            </Column>
          </Row>

          <Row>
            <Column large={4} />
            <Column large={8}>
              {checkedCrawling ? (
                <Button type="submit" disabled={submitting}>
                  Submit
                </Button>
              ) : (
                <Button type="submit">Verify</Button>
              )}
            </Column>
          </Row>
        </form>
      </section>
    );
  }

  /**
  * On submiting the form. Before making a HTTP request the source is validated using a webscrapper
  */
  _submit = values => {
    if (!this.state.checkedCrawling) {
      this._verify(values.sourcesForm);
    } else {
      values.sourcesForm.asgie = _.isEmpty(values.sourcesForm.asgie)
        ? ''
        : values.sourcesForm.asgie.value;
      values.sourcesForm.city = _.isEmpty(values.sourcesForm.city)
        ? ''
        : values.sourcesForm.city.value;
      this.props.actionCreators.postSources(values.sourcesForm);
    }
  };

  /**
  * Runs a webcrawler to validate the url and the semantic analysis
  */
  _verify = values => {
    // print the form values to the console

    let data = {
      url: `${values.url}${!_.isEmpty(values.subUrl) ? values.subUrl : ''}`,
      news_link: values.newsLink,
      news_container: values.newsContainer,
      content_container: values.contentContainer,
      content_title: values.title,
      content_description: values.description
    };

    this.setState({
      crawler:
        'Please wait while i deploy subatomic particles to crawl the web...'
    });

    new WebScrapper((err, webResults) => {
      if (err) {
        this.setState({
          crawler: JSON.stringify(err, null, ' '),
          crawlingValid: false,
          checkedCrawling: false
        });
      } else {
        this.setState({
          crawler: JSON.stringify(webResults, null, ' '),
          crawlingValid: true,
          checkedCrawling: true
        });
      }
    }).crawl(data);
  };
}

SourceAdd.propTypes = {};

SourceAdd.defaultProps = {};

/**
* a unique name(identifier) for the form
*/
SourceAdd = reduxForm({
  // a unique name for the form
  form: 'sources'
})(SourceAdd);

/**
* You have to connect() to any reducers that you wish to connect to yourself
* Connects every form based on the form name and makes sure undefined values are transformed to empty string
*/
SourceAdd = connect((state, props) => {
  let url =
    props.location.state != undefined
      ? props.location.state.historyData.url.data
      : '';

  let asgie =
    props.location.state != undefined
      ? props.location.state.historyData.rel_asgie.data.title
      : '';

  let city =
    props.location.state != undefined &&
    props.location.state.historyData.city.data !== null
      ? props.location.state.historyData.city.data.name
      : '';

  let newsContainer =
    props.location.state != undefined
      ? props.location.state.historyData.news_container.data
      : '';

  let newsLink =
    props.location.state != undefined
      ? props.location.state.historyData.news_link.data
      : '';

  let contentContainer =
    props.location.state != undefined
      ? props.location.state.historyData.content_container.data
      : '';

  let title =
    props.location.state != undefined
      ? props.location.state.historyData.content_title.data
      : '';

  let description =
    props.location.state != undefined
      ? props.location.state.historyData.content_description.data
      : '';

  return {
    initialValues: {
      sourcesForm: {
        url: url,
        asgie: _.isEmpty(asgie) ? null : { value: asgie },
        city: _.isEmpty(city) ? null : { value: city },
        newsContainer: newsContainer,
        newsLink: newsLink,
        contentContainer: contentContainer,
        title: title,
        description: description
      }
    }
  };
  // pull initial values from account reducer
})(SourceAdd);

export default SourceAdd;

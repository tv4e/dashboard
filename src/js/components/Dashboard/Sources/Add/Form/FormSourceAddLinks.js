import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  alphaNumeric,
  RenderField
} from '../../../../Form/Validator';

/**
* Source Add - News links semantics section
* Form fields to add the semantic structure to identify links
*/
let FormSourceAddLinks = props => {
  return (
    <div>
      <Field
        name="contentContainer"
        component={RenderField}
        placeholder="{tag.class} > {tag.class} "
        label="Content Container"
        type="text"
        validate={[required]}
        onChange={props.onChange}
      />
      <Field
        name="title"
        component={RenderField}
        placeholder="{tag.class} > {tag.class}"
        label="Title"
        type="text"
        validate={[required]}
        onChange={props.onChange}
      />
      <Field
        name="description"
        component={RenderField}
        placeholder="{tag.class} > {tag.class}"
        label="Description"
        type="text"
        validate={[required]}
        onChange={props.onChange}
      />
    </div>
  );
};

export default FormSourceAddLinks;

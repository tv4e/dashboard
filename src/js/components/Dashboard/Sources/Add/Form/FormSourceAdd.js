import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  url,
  RenderField,
  RenderDropdownList
} from '../../../../Form/Validator';

/**
* Source Add - Source section
* Form fields to add a source, a SubSource (not required), asgie and a city
*/
let FormSourceAdd = props => {
  let asgie = [];
  let cities = [];

  props.asgie.map((v, k) => {
    asgie.push({ value: v.title });
  });

  props.districts.map((v, k) => {
    v.cities.map((v2, k2) => {
      cities.push({ value: v2.name, district: v.name });
    });
  });

  let ValueInput = ({ item }) => {
    let separator = item.district != undefined ? `${item.district} | ` : '';

    return (
      <span>
        <strong>{separator}</strong>
        {item.value}
      </span>
    );
  };

  return (
    <div>
      <Field
        name="url"
        component={RenderField}
        type="text"
        label="News Url"
        placeholder="www.example.com"
        validate={[required, url]}
        onChange={props.onChange}
      />
      <Field
        name="subUrl"
        component={RenderField}
        label="SubSource Url"
        type="text"
        placeholder="/example"
        onChange={props.onChange}
      />
      <Field
        name="asgie"
        component={RenderDropdownList}
        label="ASGIE"
        data={asgie}
        valueField="value"
        placeholder="Select an ASGIE"
        textField="value"
        validate={[required]}
        onChange={props.onChange}
      />
      <Field
        name="city"
        component={RenderDropdownList}
        label="City"
        data={cities}
        valueComponent={ValueInput}
        groupBy={city => city.district}
        valueField="value"
        placeholder="Select a city"
        textField="value"
        onChange={props.onChange}
      />
    </div>
  );
};

export default FormSourceAdd;

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  alphaNumeric,
  RenderField
} from '../../../../Form/Validator';

/**
* Source Add - News
* Form fields to add the semantic structure to identify the news title and description
*/
let FormSourceAddNews = props => {
  return (
    <div>
      <Field
        name="newsContainer"
        component={RenderField}
        label="News Container"
        type="text"
        placeholder="{tag.class} > {tag.class}"
        validate={[required]}
        onChange={props.onChange}
      />
      <Field
        name="newsLink"
        component={RenderField}
        label="News List"
        type="text"
        placeholder="{tag.class} > {tag.class}"
        validate={[required]}
        onChange={props.onChange}
      />
    </div>
  );
};

export default FormSourceAddNews;

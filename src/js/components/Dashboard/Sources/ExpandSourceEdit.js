import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column, Icon, Button } from 'react-foundation';
import SearchFilter from '../../SearchFilter/SearchFilter';
import EditableField from '../../EditableField/EditableField';

/**
* Source edition - Sub Sources
* This section lists all the sub sources in a table. All the elements can be edited and filtered.
*/
export default class ExpandSourceEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      subSourceFilter: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { subData, onHideSubs } = this.props;
    let { subSourceFilter } = this.state;
    let subSourcesFilter = this._setSubSourcesFilter(subData.rel_subs.data);

    return (
      <section>
        <Row className="sourceEdit__search">
          <Column large={1}>
            <Button onClick={() => onHideSubs()}>
              <Icon name="fi fi-arrow-left" />
            </Button>
          </Column>
          <Column large={11}>
            <SearchFilter
              placeholder="Search Sub Source"
              filter="contains"
              data={subSourcesFilter}
              valueField="value"
              textField="value"
              onChange={subSourceFilter =>
                this.setState({ subSourceFilter: subSourceFilter.value })}
            />
          </Column>
        </Row>

        <table>
          <thead>
            <tr>
              <th>Source</th>
              <th>ASGIE</th>
              <th>City</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{subData.url.data}</td>
              <td>{subData.rel_asgie.data.title}</td>
              <td>
                {!_.isEmpty(subData.city.data)
                  ? subData.city.data.name
                  : 'None'}
              </td>
            </tr>
          </tbody>
        </table>

        <table>
          <thead>
            <tr>
              <th width="60" />
              <th width="300">Sub Sources</th>
              {subData.url.data.includes('cartasocial') && <th>Resposta</th>}
              <th>City</th>
            </tr>
          </thead>
          <tbody>
            {subData.rel_subs.data.map((v, k) => {
              v.parentSource = subData.url.data;
              if (
                subSourceFilter != null &&
                _.trim(v.url) != _.trim(subSourceFilter)
              ) {
                return null;
              }

              return (
                <tr key={`tr_${k}`}>
                  <td>
                    <Button onClick={() => this._editSubSource(v)}>Edit</Button>
                  </td>
                  <td>{v.url}</td>
                  {subData.url.data.includes('cartasocial') && (
                    <td>{v.resposta}</td>
                  )}
                  <td>{v.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    );
  }


  /**
  * When a sub source is edited, the user is redirected to the SubSource edition page which is automatically filled with the data selected
  */
  _editSubSource = data => {
    this.props.history.push({
      pathname: '/sources/add/sub',
      state: { historyData: data }
    });
  };

  /**
  * Sets the elements according to the filters
  */
  _setSubSourcesFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push({
        value: ` ${value.url}`
      });
    });

    return result;
  };
}

ExpandSourceEdit.propTypes = {};

ExpandSourceEdit.defaultProps = {};

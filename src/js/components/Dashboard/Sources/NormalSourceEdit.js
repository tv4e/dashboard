import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column, Icon, Button } from 'react-foundation';
import SearchFilter from '../../SearchFilter/SearchFilter';
import EditableField from '../../EditableField/EditableField';

/**
* Source edition - Sources
* This section lists all the sources in a table. All the elements can be edited and filtered.
*/
export default class NormalSourceEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sourceFilter: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { sourceFilter } = this.state;
    let { onShowSubs } = this.props;
    let sources = this._setSourcesData(this.props.actionData.sources.data);
    let sourcesFilter = this._setSourcesFilter(
      this.props.actionData.sources.data
    );

    return (
      <section>
        <Row className="sourceEdit__search">
          <Column>
            <SearchFilter
              placeholder="Search Source"
              filter="contains"
              data={sourcesFilter}
              valueField="value"
              textField="value"
              onChange={sourceFilter =>
                this.setState({ sourceFilter: sourceFilter.value })}
            />
          </Column>
        </Row>

        <table>
          <thead>
            <tr>
              <th />
              <th width="100">Sources</th>
              <th>Sub Source</th>
              <th>ASGIE</th>
              <th>City</th>
            </tr>
          </thead>
          <tbody>
            {sources.map((v, k) => {
              if (
                sourceFilter != null &&
                _.trim(v.url.data) != _.trim(sourceFilter)
              ) {
                return null;
              }

              return (
                <tr className="sourceEdit-tr" key={`tr_${k}`}>
                  <td>
                    <Button onClick={() => this._editSource(v)}>Edit</Button>
                  </td>
                  <td>{v.url.data}</td>
                  <td>
                    {!_.isEmpty(v.rel_subs.data) ? (
                      <a onClick={() => onShowSubs(v)}>Go to Sub Sources</a>
                    ) : (
                      'None'
                    )}
                  </td>
                  <td>{v.rel_asgie.data.title}</td>
                  <td>{!_.isEmpty(v.city.data) ? v.city.data.name : 'None'}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    );
  }

  /**
  * When a source is edited, the user is redirected to the Source edition page which is automatically filled with the data selected
  */
  _editSource = data => {
    this.props.history.push({
      pathname: '/sources/add',
      state: { historyData: data }
    });
  };

  /**
  * Sets the elements according to the filters
  */
  _setSourcesFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push({
        value: ` ${value.url}`
      });
    });

    return result;
  };

  /**
  * Sets the sources according to a specific struture
  */
  _setSourcesData = _sources => {
    let sources = [];
    let sourceObject = {};

    _sources.map((v, k) => {
      sourceObject = {};
      _.each(v, (v2, k2) => {
        sourceObject[k2] = { data: v2 };
      });
      sources.push(sourceObject);
    });

    return sources;
  };
}

NormalSourceEdit.propTypes = {};

NormalSourceEdit.defaultProps = {};

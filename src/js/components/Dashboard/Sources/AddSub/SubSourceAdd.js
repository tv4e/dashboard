import React from 'react';
import { Row, Column, Button } from 'react-foundation';
import FormSubSourceAdd from './Form/FormSubSourceAdd';
import FormSubSourceAddCity from './Form/FormSubSourceAddCity';
import FormSubSourceAddSocial from './Form/FormSubSourceAddSocial';
import WebScrapper from '../../../Tools/WebScrapper';
import _ from 'lodash';
import { connect } from 'react-redux';
import { reduxForm, FormSection } from 'redux-form';

/**
* Sub Source Add
* Component to add sub sources. Needs to meet certain requirements. News Source is required. ASGIE is required. City limits to whom the videos from the source will be sent. Depends in 3 different interconnected forms.
*/
class SubSourceAdd extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      crawling: null,
      crawlingValid: false,
      checkedCrawling: false
    };
  }

  /**
  * Makes HTTP requests to fetch the ASGIE, districts, the sources and the social services 
  */
  componentWillMount() {
    this.props.actionCreators.asgie();
    this.props.actionCreators.districts();
    this.props.actionCreators.social();
    this.props.actionCreators.sources();
  }

  /**
  * Update props with new fetched data from redux store
  */
  componentWillReceiveProps(nextProps) {
    if (
      !_.isEqual(
        this.props.actionData.postSubSources.data,
        nextProps.actionData.postSubSources.data
      )
    ) {
      if (nextProps.actionData.postSubSources.data.id != undefined) {
        alert('Success');
      } else {
        alert(nextProps.actionData.postSubSources.data);
      }
    }

    this.props = nextProps;
  }

  render() {
    let asgie = this.props.actionData.asgie.data;
    let districts = this.props.actionData.districts.data;
    let sources = this.props.actionData.sources.data;
    let social = this.props.actionData.social.data;
    let location = this.props.location;

    let { crawler, checkedCrawling } = this.state;
    let { handleSubmit, submitting } = this.props;


    /**
    * The FormSection component interconnect the 3 forms
    */
    return (
      <section className="sourceAddSub">
        <form onSubmit={handleSubmit(this._submit)}>
          <Row>
            <Column large={4}>
              <div className="title">Add a new source</div>
              <div>
                Start by adding a news source, defining the associated ASGIE and
                associating a location.
              </div>
            </Column>
            <Column large={8}>
              <FormSection name="sourcesForm">
                <FormSubSourceAdd
                  selectedSource={
                    location.state != undefined
                      ? location.state.historyData.parentSource
                      : ''
                  }
                  onChange={() => {
                    this.setState({ checkedCrawling: false });
                  }}
                  social={social}
                  asgie={asgie}
                  districts={districts}
                  sources={sources}
                />
              </FormSection>
            </Column>
          </Row>

          <hr />

          <Row>
            <Column large={4}>
              <div className="title">Define where are the news</div>
              <div>
                Enter the container for the news and then locate the title and
                body of the news.<br />If necessary add html exceptions that the
                crawler should ignore
              </div>
            </Column>
            <Column large={8}>
              <FormSection name="sourcesForm">
                <FormSubSourceAddCity
                  districts={districts}
                  onChange={() => this.setState({ checkedCrawling: false })}
                />
              </FormSection>
            </Column>
          </Row>

          <hr />

          <Row>
            <Column large={4}>
              <div className="title">Verify the crawling process</div>
              <div>
                In this area will be shown the results of the crawling process
              </div>
            </Column>
            <Column large={8}>
              <label>WebCrawler Results</label>
              <div className="sourceAddSub__crawlerResults">
                {crawler == null
                  ? 'Results of crawling will be shown here as a JSOn object'
                  : crawler}
              </div>
            </Column>
          </Row>

          <Row>
            <Column large={4} />
            <Column large={8}>
              {checkedCrawling ? (
                <Button type="submit" disabled={submitting}>
                  Submit
                </Button>
              ) : (
                <Button type="submit">Verify</Button>
              )}
            </Column>
          </Row>
        </form>
      </section>
    );
  }

  /**
  * On submiting the form. Before making a HTTP request the source is validated using a webscrapper
  */
  _submit = values => {
    if (!this.state.checkedCrawling) {
      this._verify(values.sourcesForm);
    } else {
      this.props.actionCreators.postSubSources(values.sourcesForm);
    }
  };

  /**
  * Runs a webcrawler to validate the url and the semantic analysis
  */
  _verify = values => {
    let sources = this.props.actionData.sources.data;
    let source = values.source;
    if (values.source.value != undefined) {
      source = values.source.value;
    }
    // print the form values to the console
    let data = {
      url: `${values.source}${values.url}`,
      news_link: _.find(sources, ['url', source]).news_link,
      news_container: _.find(sources, ['url', source]).news_container,
      content_container: _.find(sources, ['url', source]).content_container,
      content_title: _.find(sources, ['url', source]).content_title,
      content_description: _.find(sources, ['url', source]).content_description
    };

    this.setState({
      crawler:
        'Please wait while i deploy subatomic particles to crawl the web...'
    });

    new WebScrapper((err, webResults) => {
      if (err) {
        this.setState({
          crawler: JSON.stringify(err, null, ' '),
          crawlingValid: false,
          checkedCrawling: false
        });
      } else {
        this.setState({
          crawler: JSON.stringify(webResults, null, ' '),
          crawlingValid: true,
          checkedCrawling: true
        });
      }
    }).crawl(data);
  };
}

SubSourceAdd.propTypes = {};

SubSourceAdd.defaultProps = {};

/**
* a unique name(identifier) for the form
*/
SubSourceAdd = reduxForm({
  // a unique name for the form
  form: 'subSources'
})(SubSourceAdd);

/**
* You have to connect() to any reducers that you wish to connect to yourself
* Connects every form based on the form name and makes sure undefined values are transformed to empty string
*/
SubSourceAdd = connect((state, props) => {
  let source =
    props.location.state != undefined
      ? props.location.state.historyData.parentSource
      : '';

  let resposta =
    props.location.state != undefined
      ? props.location.state.historyData.resposta
      : '';

  let url =
    props.location.state != undefined
      ? props.location.state.historyData.url
      : '';

  let asgie =
    props.location.state != undefined
      ? props.location.state.historyData.asgie_title
      : '';

  let city =
    props.location.state != undefined
      ? props.location.state.historyData.name
      : '';

  return {
    initialValues: {
      sourcesForm: {
        source: source,
        resposta: _.isEmpty(resposta) ? null : { value: resposta },
        url: url,
        asgie: _.isEmpty(asgie) ? null : { value: asgie },
        city: _.isEmpty(city) ? null : { value: city }
      }
    }
  };
  // pull initial values from account reducer
})(SubSourceAdd);

export default SubSourceAdd;

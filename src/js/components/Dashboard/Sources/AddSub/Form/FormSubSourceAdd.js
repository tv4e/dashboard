import React from 'react';
import _ from 'lodash';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  url,
  RenderField,
  RenderDropdownList
} from '../../../../Form/Validator';

/**
* Sub Source Add - Source section
* Form fields to add a source a SubSource, asgie and carta social (not required)
*/
export default class FormSubSourceAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      source: props.selectedSource || null
    };
  }

  render() {
    let props = this.props;
    let asgie = [];
    let sources = [];
    let social = [];

    props.asgie.map((v, k) => {
      asgie.push({ value: v.title });
    });

    props.sources.map((v, k) => {
      sources.push({ value: v.url });
    });

    props.social.map((v, k) => {
      social.push({ value: v.resposta });
    });

    return (
      <div>
        <Field
          name="source"
          component={RenderDropdownList}
          label="Source"
          data={sources}
          valueField="value"
          placeholder="Select a source"
          textField="value"
          ref={ref => (this.field = ref)}
          validate={[required]}
          onChange={source => {
            props.onChange();
            this.setState({ source: source.value });
          }}
        />
        {_.includes(this.state.source, 'cartasocial') && (
          <Field
            name="resposta"
            component={RenderDropdownList}
            label="CartaSocial"
            data={social}
            valueField="value"
            placeholder="Select a carta Social"
            textField="value"
            validate={[required]}
            onChange={props.onChange}
          />
        )}
        <Field
          name="url"
          component={RenderField}
          type="text"
          label="Sub Source Url"
          placeholder="www.example.com"
          validate={[required]}
          onChange={props.onChange}
        />
        <Field
          name="asgie"
          component={RenderDropdownList}
          label="ASGIE"
          data={asgie}
          valueField="value"
          placeholder="Select an ASGIE"
          textField="value"
          validate={[required]}
          onChange={props.onChange}
        />
      </div>
    );
  }
}

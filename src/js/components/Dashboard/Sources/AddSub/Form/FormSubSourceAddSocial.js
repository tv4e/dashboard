import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  alphaNumeric,
  RenderField
} from '../../../../Form/Validator';

/**
* Sub Source Add - Semantic section
* Form fields to add the semantic structure for the webscrapper
*/
let FormSubSourceAddSocial = props => {
  return (
    <div>
      <Field
        name="content_title"
        component={RenderField}
        placeholder="{tag.class} > {tag.class}"
        label="Title"
        type="text"
        validate={[required]}
        onChange={props.onChange}
      />
      <Field
        name="content_description"
        component={RenderField}
        placeholder="{tag.class} > {tag.class}"
        label="Description"
        type="text"
        validate={[required]}
        onChange={props.onChange}
      />
    </div>
  );
};

export default FormSubSourceAddSocial;

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  url,
  RenderField,
  RenderDropdownList
} from '../../../../Form/Validator';

/**
* Sub Source Add - City section
* Form fields to add a city to a SubSource
*/
let FormSubSourceAddCity = props => {
  let cities = [];

  props.districts.map((v, k) => {
    v.cities.map((v2, k2) => {
      cities.push({ value: v2.name, district: v.name });
    });
  });

  let ValueInput = ({ item }) => {
    let separator = item.district != undefined ? `${item.district} | ` : '';

    return (
      <span>
        <strong>{separator}</strong>
        {item.value}
      </span>
    );
  };

  return (
    <div>
      <Field
        name="city"
        component={RenderDropdownList}
        label="City"
        data={cities}
        valueComponent={ValueInput}
        groupBy={city => city.district}
        valueField="value"
        placeholder="Select a city"
        textField="value"
        onChange={props.onChange}
      />
    </div>
  );
};

export default FormSubSourceAddCity;

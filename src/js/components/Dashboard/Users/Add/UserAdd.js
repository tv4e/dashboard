import React from 'react';
import { Row, Column, Button } from 'react-foundation';
import Form from './Form/FormUserAdd';
import _ from 'lodash';

/**
* User add
* Component to add users. The form is a child component
*/
export default class UserAdd extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
  * Before mounting component makes a HTTP request to get the ASGIE and cities
  */
  componentWillMount() {
    this.props.actionCreators.asgie();
    this.props.actionCreators.districts();
  }

  /**
  * Updates Props based on actionData
  */
  componentWillReceiveProps(nextProps) {
    if (
      !_.isEqual(
        this.props.actionData.postBox.data,
        nextProps.actionData.postBox.data
      )
    ) {
      if (nextProps.actionData.postBox.data.id != undefined) {
        alert('Success');
      } else {
        alert(nextProps.actionData.postBox.data);
      }
    }

    this.props = nextProps;
  }

  render() {
    let districts = this.props.actionData.districts.data;

    return (
      <section className="usersAdd">
        <Row>
          <Column large={4}>
            <div className="title">Add a new User</div>
            <div>Add the information necessary for creating a new Box.</div>
          </Column>
          <Column large={8}>
            <Form districts={districts} onSubmit={this._submit} />
          </Column>
        </Row>
      </section>
    );
  }

  /**
  * HTTP request to add the user
  */
  _submit = values => {
    values.city = values.city.value;
    // print the form values to the console
    this.props.actionCreators.postBox(values);
  };
}

UserAdd.propTypes = {};

UserAdd.defaultProps = {};

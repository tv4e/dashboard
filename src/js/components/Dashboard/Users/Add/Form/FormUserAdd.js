import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
  required,
  number,
  alphaNumeric,
  minValue,
  maxValue,
  RenderField,
  RenderDropdownList
} from '../../../../Form/Validator';
import { Button } from 'react-foundation';

/**
* User Add
* Form fields to add a user and its demographic characteristics.
*/
let FormUserAdd = props => {
  const { handleSubmit, pristine, reset, submitting } = props;
  let cities = [];

  props.districts.map((v, k) => {
    v.cities.map((v2, k2) => {
      cities.push({ value: v2.name, district: v.name });
    });
  });

  let ValueInput = ({ item }) => {
    let separator = item.district != undefined ? `${item.district} | ` : '';

    return (
      <span>
        <strong>{separator}</strong>
        {item.value}
      </span>
    );
  };

  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="uu_id"
        component={RenderField}
        type="text"
        placeholder="Type a UU_ID"
        label="Box UU_ID"
        validate={[required, number, minValue(1)]}
      />

      <Field
        name="user"
        component={RenderField}
        type="text"
        placeholder="Type the user name"
        label="User Name"
        validate={[required]}
        // warn={alphaNumeric}
      />

      <Field
        name="gender"
        component={RenderDropdownList}
        label="gender"
        data={['M', 'F']}
        placeholder="Select a Gender"
        textField="value"
        validate={[required]}
      />

      <Field
        name="age"
        component={RenderField}
        label="Age"
        placeholder="Age must be 65 or above"
        type="text"
        validate={[required, number, minValue(65), maxValue(150)]}
      />

      <Field
        name="serial"
        component={RenderField}
        type="text"
        placeholder="Get the serial from the set top box"
        label="Serial"
        validate={[required, alphaNumeric]}
      />

      <Field
        name="city"
        component={RenderDropdownList}
        label="City"
        data={cities}
        valueComponent={ValueInput}
        groupBy={city => city.district}
        valueField="value"
        placeholder="Select a city"
        textField="value"
        onChange={props.onChange}
      />

      <Field
        name="coordinates"
        component={RenderField}
        type="text"
        label="Coordinates"
        placeholder="Longitude and latitude"
        validate={[required]}
        // warn={alphaNumeric}
      />

      <Field
        name="iptv_type"
        component={RenderDropdownList}
        label="Iptv type"
        data={['iptv1', 'iptv2', 'iptv_teste']}
        placeholder="Select a video reception mode"
        textField="value"
        validate={[required]}
      />

      <Button type="submit" disabled={submitting}>
        Submit
      </Button>
    </form>
  );
};

/**
* a unique name(identifier) for the form
*/
FormUserAdd = reduxForm({
  // a unique name for the form
  form: 'user'
})(FormUserAdd);

export default FormUserAdd;

import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column, Icon } from 'react-foundation';
import SearchFilter from '../../SearchFilter/SearchFilter';
import EditableField from '../../EditableField/EditableField';
import EditableDropdown from '../../EditableField/EditableDropdown';

/**
* User Edit 
* Component to edit users. All the data is displayed on table where the items can be edited. 
*/
export default class UserEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userFilter: null,
      locationFilter: null,
      districtFilter: null
    };
  }

  /**
  * Before mounting component makes a HTTP request to get the boxes and cities
  */
  componentWillMount() {
    this.props.actionCreators.boxes();
    this.props.actionCreators.districts();
  }

  /**
  * Updates Props based on redux actionData
  */
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { userFilter, locationFilter, districtFilter } = this.state;
    let boxesFilter = this._setBoxesFilter(this.props.actionData.boxes.data);
    let districtsFilter = this._setDistrictsFilter(
      this.props.actionData.districts.data
    );
    let locationsFilter = this._setLocationFilter(
      this.props.actionData.boxes.data
    );
    let boxes = this._filterUsers(this.props.actionData.boxes.data);

    return (
      <section className="usersEdit">
        <Row>
          <Column>
            <Row className="usersEdit__search">
              <Column>
                <SearchFilter
                  placeholder="Select User"
                  filter="contains"
                  data={boxesFilter}
                  valueField="value"
                  textField="value"
                  onChange={userFilter =>
                    this.setState({ userFilter: userFilter.value })}
                />
              </Column>
              <Column>
                <SearchFilter
                  name="city"
                  filter="contains"
                  data={districtsFilter}
                  groupBy={city => city.district}
                  valueField="value"
                  placeholder="Search a city"
                  textField="value"
                  onChange={districtFilter =>
                    this.setState({ districtFilter: districtFilter.value })}
                />
              </Column>
              <Column>
                <SearchFilter
                  placeholder="Search Location"
                  data={locationsFilter}
                  onChange={locationFilter =>
                    this.setState({ locationFilter: locationFilter.value })}
                />
              </Column>
            </Row>

            <div className="table_scroll">
              <table>
                <thead>
                  <tr>
                    <th>Box ID</th>
                    <th>Box UUID</th>
                    <th>Interface</th>
                    <th>User</th>
                    <th>Gender</th>
                    <th>Serial</th>
                    <th>Last Channel</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Location</th>
                  </tr>
                </thead>
                <tbody>
                  {boxes.map((v, k) => {
                    return (
                      <tr key={`tr_${k}`}>
                        <td>
                          {v.id} <a>Reset</a>
                        </td>
                        <td>
                          <EditableField
                            valueUnselected={v.uu_id}
                            valueSelected={v.uu_id}
                            onSubmit={data => this._onSubmit(data)}
                            name={'uu_id'}
                          />
                        </td>
                        <td style={{ display: 'block', width: '100px' }}>
                          <a
                            onClick={() => this._onInterface(v.iptv_type, v.id)}
                          >
                            {v.iptv_type == 'iptv'
                              ? 'notification'
                              : 'injection'}
                          </a>
                        </td>
                        <td>
                          <EditableField
                            valueUnselected={v.user}
                            valueSelected={v.user}
                            onSubmit={data => this._onUserName(data, v.id)}
                            name={'user'}
                          />
                        </td>
                        <td>
                          <a onClick={() => this._onGender(v.gender, v.id)}>
                            {v.gender}
                            &nbsp;
                            {v.gender == 'M' ? (
                              <Icon name="fi-male-symbol" />
                            ) : (
                              <Icon name="fi-female-symbol" />
                            )}
                          </a>
                        </td>
                        <td>
                          {v.serial}
                          &nbsp;
                          <Link
                            to={`http://app.tv4e.pt/?id=${v.serial}`}
                            target="_blank"
                          >
                            <Icon name="fa fa-external-link" />
                          </Link>
                        </td>
                        <td>{v.last_channel}</td>
                        <td>
                          {v.on_state == 0 ? 'OFF' : 'ON'}
                          {/*<a onClick={() => this._onState(v.on_state, v.id)}>*/}
                          {/*{v.on_state == 0 ? 'OFF' : 'ON'}*/}
                          {/*</a>*/}
                        </td>
                        <td style={{ display: 'block', width: '250px' }}>
                          <EditableDropdown
                            name="city"
                            filter="contains"
                            data={districtsFilter}
                            groupBy={city => city.district}
                            placeholder="Search a city"
                            valueField="value"
                            textField="value"
                            defaultValue={v.city}
                            valueUnselected={v.city}
                            onSubmit={data => this._onCity(data, v.id)}
                          />
                        </td>
                        <td>
                          <EditableField
                            valueUnselected={v.coordinates}
                            valueSelected={v.coordinates}
                            onSubmit={data => this._onLocation(data, v.id)}
                            name="coordinates"
                          />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </Column>
        </Row>
      </section>
    );
  }

  /**
  * Filters by box
  */ 
  _setBoxesFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push({
        separator: `Box tag ${value.uu_id}`,
        value: ` ${value.user}`,
        prefix: `Box tag ${value.uu_id} |`
      });
    });

    return result;
  };

  /**
  * Filters by location
  */ 
  _setLocationFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push(value.coordinates);
    });

    return result;
  };

  /**
  * Filters by city
  */ 
  _setDistrictsFilter = data => {
    let result = [];

    data.map((v, k) => {
      v.cities.map((v2, k2) => {
        result.push({ value: v2.name, district: v.name, separator: v.name });
      });
    });

    return result;
  };

  /**
  * Filters by users
  */ 
  _filterUsers = data => {
    let { userFilter, locationFilter, districtFilter } = this.state;
    let values = data;

    if (userFilter) {
      values = _.filter(values, function(v) {
        return _.trim(v.user) == _.trim(userFilter);
      });
    }

    if (locationFilter) {
      values = _.filter(values, function(v) {
        return _.trim(v.coordinates) == _.trim(locationFilter);
      });
    }

    if (districtFilter) {
      values = _.filter(values, function(v) {
        return _.trim(v.city) == _.trim(districtFilter);
      });
    }

    return values;
  };

 /**
 * HTTP request to change users box onstate
 */
  _onState = (value, id) => {
    let state = Math.abs(value - 1);
    this.props.actionCreators.boxState({ on_state: state }, id);
  };

   /**
   * HTTP request to change users username
   */
  _onUserName = (value, id) => {
    this.props.actionCreators.boxState({ user: value.user }, id);
  };

   /**
   * HTTP request to change the IPTV type
   */
  _onInterface = (value, id) => {
    let _value = value === 'iptv' ? 'iptv2' : 'iptv';
    this.props.actionCreators.boxState({ iptv_type: _value }, id);
  };

 /**
 * HTTP request to change users gender
 */
  _onGender = (value, id) => {
    let state = value == 'M' ? 'F' : 'M';
    this.props.actionCreators.boxState({ gender: state }, id);
  };

 /**
 * HTTP request to change users city
 */
  _onCity = (value, id) => {
    this.props.actionCreators.boxState({ city: value.value }, id);
  };

 /**
 * HTTP request to change users coordinates
 */
  _onLocation = (value, id) => {
    this.props.actionCreators.boxState({ coordinates: value.coordinates }, id);
  };
}

UserEdit.propTypes = {};

UserEdit.defaultProps = {};

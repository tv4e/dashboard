import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column } from 'react-foundation';

/**
* Home Screen
* Initial screen shown when loggin in to the dashboard
*/
export default class Home extends React.Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <section className="home">
        <h2>Welcome to the +TV4E management dashboard!</h2>
        <br />
        <h5>Navigate through the system using the menu on your left !</h5>
        <br />
        <br />
        <img
          src="http://www.pvhc.net/img199/jsubpvhxokkbhrmrhiie.png"
          alt="dashboard_icon"
          width={200}
          height={200}
        />
      </section>
    );
  }
}

Home.propTypes = {};

Home.defaultProps = {};

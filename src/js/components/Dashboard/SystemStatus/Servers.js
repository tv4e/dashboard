import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column } from 'react-foundation';
import _ from 'lodash';

import health00to19 from './../../../../assets/icons/health-00to19.png';
import health20to39 from './../../../../assets/icons/health-20to39.png';
import health40to59 from './../../../../assets/icons/health-40to59.png';
import health60to79 from './../../../../assets/icons/health-60to79.png';
import health80plus from './../../../../assets/icons/health-80plus.png';

/**
  * Server state
  * Provides an interface to see the state of the available services in Jenkins and server status
*/  
export default class Servers extends React.Component {
  constructor(props) {
    super(props);
  }


  /**
  * Fetches the jenkins services information with an HTTP request
  */  
  componentWillMount() {
    this.props.actionCreators.jenkinsVideoCron();
    this.props.actionCreators.jenkinsDeleteCron();
    // this.props.actionCreators.jenkinsWeb();
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let video_cron = this.props.actionData.jenkinsVideoCron.data || [];
    let video_cron_HealthIcon = !_.isEmpty(video_cron)
      ? this._healthReportIcon(video_cron.healthReport[0].iconUrl)
      : [];
    let delete_cron = this.props.actionData.jenkinsDeleteCron.data || [];
    let delete_cron_HealthIcon = !_.isEmpty(delete_cron)
      ? this._healthReportIcon(delete_cron.healthReport[0].iconUrl)
      : [];

    console.log(delete_cron);
    return (
      <section className="servers">
        <Row>
          <Column>
            <div className="servers__column">
              <span className="title"> SERVER 1 </span>
              <br />
              API
            </div>
          </Column>

          <Column>
            <div className="servers__column">
              <span className="title"> SERVER 2 </span>
              <br />
              Video Builder | CDN
              <br />
              <img src={video_cron_HealthIcon} />
              {video_cron.fullDisplayName}
              <br />
              <img src={delete_cron_HealthIcon} />
              {delete_cron.fullDisplayName}
            </div>
          </Column>

          <Column>
            <div className="servers__column">
              <span className="title"> SERVER 3 </span>
              <br />
              <div className="switch">
                <input checked type="checkbox" name="recommendation_system" />
              </div>
              Recommendation System
            </div>
          </Column>
        </Row>
      </section>
    );
  }

 /**
 * Sets the health icon according to the jenkins service build states
 */
  _healthReportIcon = icon => {
    switch (icon) {
      case 'health-00to19.png':
        return health00to19;
        break;
      case 'health-20to39.png':
        return health20to39;
        break;
      case 'health-40to59.png':
        return health40to59;
        break;
      case 'health-60to79.png':
        return health60to79;
        break;
      case 'health-80plus.png':
        return health80plus;
        break;
    }
  };
}

Servers.propTypes = {};

Servers.defaultProps = {};

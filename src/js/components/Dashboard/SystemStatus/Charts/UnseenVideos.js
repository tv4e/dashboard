import React from 'react';
import {
  XAxis,
  Tooltip,
  CartesianGrid,
  Line,
  LineChart,
  Bar,
  BarChart,
  YAxis,
  Legend,
  Brush
} from 'recharts';
import _ from 'lodash';
import Moment from 'moment';

/**
* Charts - Unseen Videos
* Display the Charts of the using the recharts package
*/
export default class UnseenVideos extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
  * Updates props from the redux store
  */  
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { sortBy, data } = this.props;

    let chart = [];
    if (this.props.data != undefined) {
      if (sortBy === 'ASGIE') {
        chart = this._sortByAsgie(data.data);
      }
      if (sortBy === 'Date') {
        chart = this._sortByDate(data.data);
      }
    }

    return <div>{chart}</div>;
  }

  /**
  * A function to sort and struture the component based on the Asgie from A to Z
  */
  _sortByAsgie = data => {
    let { chartType } = this.props;
    let bars = [];
    let lines = [];
    let _data = [];
    let chart;

    /**
    * If statement for BarChart and LineChart
    */
    _.forEach(data, (v, k) => {
      if (chartType === 'BarChart') {
        bars.push(
          <Bar
            key={v.asgie_title_pt}
            dataKey={v.asgie_title_pt}
            fill={v.asgie_color}
            cursor="pointer"
            onClick={data => this.props.onClick(data.details)}
          />
        );
      } else {
        lines.push(
          <Line
            type="monotone"
            key={v.asgie_title_pt}
            dataKey={v.asgie_title_pt}
            stroke={v.asgie_color}
            activeDot={{ r: 8 }}
          />
        );
      }

      _data.push({
        name: v.asgie_title_pt,
        [v.asgie_title_pt]: v.data.length,
        videos: v.data.length,
        details: v.data
      });
    });

   /**
    * If statement for BarChart and LineChart
    */
    if (chartType === 'BarChart') {
      chart = (
        <BarChart
          width={600}
          height={300}
          data={_data}
          margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
          barSize={50}
        >
          <XAxis dataKey="name" hide={true} />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend
            margin={{ top: 5, left: 10, right: 5, bottom: 0 }}
            align="right"
            verticalAlign="middle"
            layout="vertical"
          />
          {bars}
        </BarChart>
      );
    } else {
      chart = (
        <LineChart
          width={600}
          height={300}
          data={_data}
          margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
          barSize={50}
        >
          <XAxis dataKey="name" hide={true} />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend
            margin={{ top: 5, left: 10, right: 5, bottom: 0 }}
            align="right"
            verticalAlign="middle"
            layout="vertical"
          />
          <Line
            type="monotone"
            dataKey="videos"
            stroke="#8884d8"
            activeDot={{ r: 0 }}
          />
          {lines}
        </LineChart>
      );
    }

    return chart;
  };

  /**
  * A function to sort and struture the component based on a date
  */
  _sortByDate = data => {
    let { chartType } = this.props;
    let _data = [];
    let _data2 = [];
    let _barData = [];
    let bars = [];
    let chart;

    /**
    * Organizes the data for the charts
    */
    _.each(data, (v, k) => {
      _.each(v.data, (v2, k2) => {
        _data.push({
          date: Moment(v2.date).format('YYYY-MM-DD'),
          data: {
            asgie_title_pt: v.asgie_title_pt,
            asgie_title: v.asgie_title,
            video_title: v2.video_title
          }
        });
      });
    });

    _.each(_data, value => {
      if (_data2[_.findIndex(_data2, { date: value.date })]) {
        let dateObj = _data2[_.findIndex(_data2, { date: value.date })];

        dateObj.data.videos.push({
          asgie_title: value.data.asgie_title,
          asgie_title_pt: value.data.asgie_title_pt,
          title: value.data.video_title
        });
      } else {
        _data2.push({
          date: value.date,
          data: {
            videos: [
              {
                asgie_title: value.data.asgie_title,
                asgie_title_pt: value.data.asgie_title_pt,
                title: value.data.video_title
              }
            ]
          }
        });
      }
    });

    _data2 = _.orderBy(_data2, ['date'], ['asc']);

    _.each(_data2, (v, k) => {
      _barData.push({
        name: v.date,
        videos: v.data.videos.length,
        details: v.data.videos
      });
    });

    /**
    * If statement for BarChart and LineChart
    */
    if (chartType === 'BarChart') {
      chart = (
        <BarChart width={600} height={300} data={_barData}>
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Brush dataKey="name" height={30} stroke="#8884d8" />
          <Bar
            cursor="pointer"
            onClick={data => this.props.onClick(data.details)}
            dataKey="videos"
            fill="#8884d8"
          />
        </BarChart>
      );
    } else {
      chart = (
        <LineChart width={600} height={300} data={_barData}>
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Brush dataKey="name" height={30} stroke="#8884d8" />
          <Line
            type="monotone"
            dataKey="videos"
            stroke="#8884d8"
            activeDot={{ r: 8 }}
          />
        </LineChart>
      );
    }

    return chart;
  };
}

UnseenVideos.propTypes = {};

UnseenVideos.defaultProps = {};

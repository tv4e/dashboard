import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column } from 'react-foundation';
import _ from 'lodash';
import SearchFilter from '../../SearchFilter/SearchFilter';
import CalendarFilter from '../../SearchFilter/CalendarFilter';
import AnalysisBoard from './AnalysisBoard';
import { Range } from 'rc-slider';

/**
  * Usage Analytics
  * Provides an interface to filter charts according to users, date, type of video injection and user interactions
*/  
export default class UsageAnalysis extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      from: null,
      to: null,
      type: null,
      event: null,
      rating: null,
      userFilter: null,
      dataKey: null,
      seenRange: [1, 100],
      charts: null
    };
  }

  /**
  * Fetches the boxes information with an HTTP request
  */  
  componentWillMount() {
    this.props.actionCreators.boxes();
  }

  /**
  * Updates props from redux store
  */  
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { dataKey, charts, seenRange, event } = this.state;
    let { actionData } = this.props;

    let chartData = {
      rewatched: actionData.rewatchedLibrary.data,
      seen: actionData.seen.data,
      unseen: actionData.unseen.data
    };

    let data = {
      notified: ['seen', 'unseen', 'seen library', 'rewatched library'],
      injected: ['seen', 'unseen', 'seen library', 'rewatched library']
    };

    let boxesRaw = this.props.actionData.boxes.data;
    let boxes = _.values(this._filterCharts(this.props.actionData.boxes.data));

    console.log(chartData);

    return (
      <section className="usageAnalysis">
        <Row>
          <Column>
            <Row className="usageAnalysis__search">
              <Column>
                <SearchFilter
                  placeholder="Select seen type"
                  data={['notified', 'injected']}
                  onChange={value => {
                    this.setState({ type: value, dataKey: value }, () => {
                      this._callCharts();
                    });
                  }}
                />
              </Column>
              <Column>
                <SearchFilter
                  placeholder="Select interaction event"
                  data={data[dataKey]}
                  onChange={value => {
                    this.setState({ event: value }, () => {
                      this._callCharts();
                    });
                  }}
                />
              </Column>
              {event == 'seen' && (
                <Column>
                  <SearchFilter
                    placeholder="Select rating value"
                    data={['-1', '0', '1']}
                    onChange={value => {
                      this.setState({ rating: value }, () => {
                        this._callCharts();
                      });
                    }}
                  />
                </Column>
              )}
              {event == 'seen' && (
                <Column>
                  Select seen percentage
                  <Range
                    min={1}
                    max={100}
                    allowCross={false}
                    value={seenRange}
                    marks={{
                      [seenRange[0]]: `${[seenRange[0]]}%`,
                      [seenRange[1]]: `${[seenRange[1]]}%`
                    }}
                    onAfterChange={() => this._callCharts()}
                    onChange={value => this.setState({ seenRange: value })}
                  />
                </Column>
              )}
            </Row>
            <Row className="usageAnalysis__search">
              <Column>
                <SearchFilter
                  placeholder="Select User"
                  filter="contains"
                  data={this._setBoxesFilter(boxesRaw)}
                  valueField="value"
                  textField="value"
                  onChange={value => {
                    console.log(value);
                    this.setState(
                      { userFilter: value ? value.value : null },
                      () => {
                        this._callCharts();
                      }
                    );
                  }}
                />
              </Column>
              <Column>
                <CalendarFilter
                  onChange={value => {
                    this.setState(value, () => {
                      this._callCharts();
                    });
                  }}
                />
              </Column>
            </Row>

            {charts != null
              ? boxes.map((v, k) => {
                  let iptv_type =
                    v.iptv_type === 'iptv' ? 'notification' : 'injection';

                  return (
                    <AnalysisBoard
                      key={`analysisBoard_${k}`}
                      userData={[
                        { title: 'UU_ID', data: v.uu_id },
                        { title: 'User', data: v.user },
                        { title: 'ReceptionType', data: iptv_type },
                        { title: 'City', data: v.city },
                        { title: 'Location', data: v.coordinates }
                      ]}
                      chartData={_.find(chartData[charts].data, {
                        box_id: v.id
                      })}
                      charts={charts}
                      box_id={v.id}
                    />
                  );
                })
              : 'Select the adequate filters to create charts, FROM field is required'}
          </Column>
        </Row>
      </section>
    );
  }

  /**
  * Sets the data for the filter to select boxes.
  */  
  _setBoxesFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push({
        separator: `Box tag ${value.uu_id}`,
        value: ` ${value.user}`,
        prefix: `Box tag ${value.uu_id} |`
      });
    });

    return result;
  };

  /**
  * Sets the charts according to the set filters by fetching their data with an HTTP request
  */  
  _callCharts = () => {
    let { from, to, box, event, type, rating, seenRange } = this.state;

    let charts;
    //for is an object of DATE TIME so isEmpty wont work
    if (from && !_.isEmpty(event) && !_.isEmpty(type)) {
      if (type == 'notified' && event == 'seen') {
        this.props.actionCreators.seen({
          type,
          from,
          to,
          rating,
          seenFrom: seenRange[0],
          seenTo: seenRange[1]
        });
        charts = 'seen';
      }
      if (type == 'notified' && event == 'unseen') {
        this.props.actionCreators.unseen({
          type,
          from,
          to
        });
        charts = 'unseen';
      }
      if (type == 'notified' && event == 'seen library') {
        this.props.actionCreators.seen({
          type: 'library',
          from,
          to,
          rating,
          seenFrom: seenRange[0],
          seenTo: seenRange[1]
        });
        charts = 'seen';
      }
      if (type == 'notified' && event == 'rewatched library') {
        this.props.actionCreators.rewatchedLibrary({
          type,
          from,
          to,
          rating
        });
        charts = 'rewatched';
      }
      if (type == 'injected' && event == 'seen') {
        this.props.actionCreators.seen({
          type,
          from,
          to,
          rating,
          seenFrom: seenRange[0],
          seenTo: seenRange[1]
        });
        charts = 'seen';
      }
      if (type == 'injected' && event == 'seen library') {
        this.props.actionCreators.seen({
          type: 'library',
          from,
          to,
          rating
        });
        charts = 'seen';
      }
      if (type == 'injected' && event == 'unseen') {
        this.props.actionCreators.unseen({
          type,
          from,
          to
        });
        charts = 'unseen';
      }
      if (type == 'injected' && event == 'rewatched library') {
        this.props.actionCreators.rewatchedLibrary({
          type,
          from,
          to,
          rating
        });
        charts = 'rewatched';
      }
    }
    this.setState({ charts });
  };

  /**
  * Filters the available charts according to users
  */
  _filterCharts = data => {
    let { userFilter } = this.state;
    let values = data;

    if (userFilter) {
      values = _.filter(values, function(v) {
        return _.trim(v.user) == _.trim(userFilter);
      });
    }
    return values;
  };
}

UsageAnalysis.propTypes = {};

UsageAnalysis.defaultProps = {};

import React from 'react';
import { Row, Column } from 'react-foundation';
import DropdownList from 'react-widgets/lib/DropdownList';
import Seen from './Charts/SeenVideos';
import Unseen from './Charts/UnseenVideos';
import RewatchedVideos from './Charts/RewatchedVideos';
import _ from 'lodash';
import Collapsible from 'react-collapsible';

 /**
 * Analysis Board 
 * Each users analysis board. Featuring their data and their charts
 */
export default class AnalysisBoard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sortBy: 'ASGIE',
      chartType: 'BarChart',
      details: null
    };
  }

  /**
  * Fetches the boxes information with an HTTP request
  */  
  componentWillReceiveProps(nextProps, nextState) {
    this.props = nextProps;
  }

  /**
  * Fetches the boxes information with an HTTP request
  */  
  render() {
    let { userData, charts, chartData } = this.props;
    let { sortBy, chartType, details } = this.state;

    return (
      <section className="analysisBoard">
        <Row>
          <Column large={4}>
            <div className="title">USER DATA</div>
            {userData.map((v, k) => {
              return (
                <div key={`board_${k}`}>
                  <strong>{v.title}</strong> : {v.data}
                </div>
              );
            })}
            <br />
            <strong>Details</strong> :
            <div className="analysisBoard__details">
              {details === null ? (
                <div>
                  <strong>Click</strong> on a <strong>chart item</strong> to
                  show details
                </div>
              ) : (
                details
              )}
            </div>
          </Column>

          <Column large={8}>
            <Row className="analysisBoard__charts">
              <Column large={6}>
                <div className="title">
                  Chart type:
                  <DropdownList
                    data={['BarChart', 'LineChart']}
                    onChange={value => this.setState({ chartType: value })}
                    defaultValue="BarChart"
                  />
                </div>
              </Column>

              <Column large={6}>
                <div className="title">
                  Sort by:
                  <DropdownList
                    data={['ASGIE', 'Date']}
                    onChange={value => this.setState({ sortBy: value })}
                    defaultValue="ASGIE"
                  />
                </div>
              </Column>
            </Row>

            {charts === 'seen' && (
              <Seen
                onClick={data => this._setDetails(data)}
                chartType={chartType}
                sortBy={sortBy}
                data={chartData}
              />
            )}
            {charts === 'rewatched' && (
              <RewatchedVideos
                onClick={data => this._setDetails(data)}
                chartType={chartType}
                sortBy={sortBy}
                data={chartData}
              />
            )}
            {charts === 'unseen' && (
              <Unseen
                onClick={data => this._setDetails(data)}
                chartType={chartType}
                sortBy={sortBy}
                data={chartData}
              />
            )}
          </Column>
        </Row>
      </section>
    );
  }

  _setDetails = details => {
    let { charts } = this.props;
    let { sortBy } = this.state;
    let _details = [];

    if (charts === 'rewatched') {
      if (sortBy === 'ASGIE') {
        _.each(details, (v, k) => {
          _details.push(
            <Collapsible key={`collapsible_${k}`} trigger={v.video_title}>
              {v.rewatchedHistory.map((v2, k2) => {
                return <p key={`collapsible_${k}_paragraph${k2}`}>{v2}</p>;
              })}
            </Collapsible>
          );
        });
      }
      if (sortBy === 'Date') {
        let arrayData = [];
        _.each(details, (v, k) => {
          if (
            k === 0 ||
            v.asgie_title != arrayData[arrayData.length - 1].asgie_title
          ) {
            arrayData.push({
              asgie_title: v.asgie_title,
              data: [
                <p key={`collapsibleSepItem_${k}`}>
                  <strong>{v.title}</strong> : rewatched {v.rewatched}x
                </p>
              ]
            });
          } else {
            arrayData[arrayData.length - 1].data.push(
              <p key={`collapsibleSepItem_${k}`}>
                <strong>{v.title}</strong> : rewatched {v.rewatched}x
              </p>
            );
          }
        });

        _.each(arrayData, (v, k) => {
          _details.push(
            <Collapsible key={`collapsible_${k}`} trigger={v.asgie_title}>
              {v.data}
            </Collapsible>
          );
        });
      }
    } else {
      if (sortBy === 'ASGIE') {
        _.each(details, (v, k) => {
          _details.push(
            <Collapsible key={`collapsible_${k}`} trigger={v.video_title}>
              <p>{v.date}</p>
            </Collapsible>
          );
        });
      }
      if (sortBy === 'Date') {
        details = _.orderBy(details, ['asgie_title'], ['asc']);

        let arrayData = [];
        _.each(details, (v, k) => {
          if (
            k === 0 ||
            v.asgie_title != arrayData[arrayData.length - 1].asgie_title
          ) {
            arrayData.push({
              asgie_title: v.asgie_title,
              data: [<p key={`collapsibleSepItem_${k}`}>{v.title}</p>]
            });
          } else {
            arrayData[arrayData.length - 1].data.push(
              <p key={`collapsibleSepItem_${k}`}>{v.title}</p>
            );
          }
        });

        _.each(arrayData, (v, k) => {
          _details.push(
            <Collapsible key={`collapsible_${k}`} trigger={v.asgie_title}>
              {v.data}
            </Collapsible>
          );
        });
      }
    }

    this.setState({ details: _details });
  };
}

AnalysisBoard.propTypes = {};

AnalysisBoard.defaultProps = {};

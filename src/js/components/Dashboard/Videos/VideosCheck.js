import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column, Icon } from 'react-foundation';
import SearchFilter from '../../SearchFilter/SearchFilter';
import Calendar from '../../SearchFilter/CalendarFilter';
import Moment from 'moment';
import PageIndexer from './../../PageIndexer/PageIndexer';

/**
* Check Videos
* Component to all the created videos
*/
export default class VideosCheck extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      titleFilter: null,
      asgieFilter: null,
      fromFilter: null,
      toFilter: null,
      pageIndex: 0
    };
  }


  /**
  * Before mounting component makes a HTTP request to get the asgie and the created videos
  */
  componentWillMount() {
    this.props.actionCreators.videos();
    this.props.actionCreators.asgie();
  }

  /**
  * Updates Props based on redux actionData
  */
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  render() {
    let { pageIndex } = this.state;
    let videosFilter = this._setVideosFilter(this.props.actionData.videos.data);
    let asgiesFilter = this._setAsgieFilter(this.props.actionData.asgie.data);
    let videos =
      _.values(this._filterVideos(this.props.actionData.videos.data)) || [];

    return (
      <section className="usersEdit">
        <Row>
          <Column>
            <Row className="usersEdit__search">
              <Column>
                <Calendar
                  onChange={value => {
                    this.setState({
                      fromFilter: value.from,
                      toFilter: value.to
                    });
                  }}
                />
              </Column>

              <Column>
                <SearchFilter
                  placeholder="Select a Title"
                  filter="contains"
                  data={videosFilter}
                  onChange={titleFilter => {
                    this.setState({ titleFilter: titleFilter.value });
                  }}
                />
              </Column>

              <Column>
                <SearchFilter
                  placeholder="Select an ASGIE"
                  filter="contains"
                  data={asgiesFilter}
                  onChange={asgieFilter =>
                    this.setState({ asgieFilter: asgieFilter.value })}
                />
              </Column>
            </Row>

            <PageIndexer
              pageNumber={_.chunk(videos, 5).length}
              setPageIndex={pageIndex =>
                this.setState({ pageIndex: pageIndex })}
            />

            <table>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Asgie</th>
                  <th>Created</th>
                </tr>
              </thead>
              <tbody>
                {!_.isEmpty(videos) &&
                  _.chunk(videos, 5)[pageIndex].map((v, k) => {
                    return (
                      <tr key={`tr_${k}`}>
                        <td>{v.video_title}</td>
                        <td>{v.video_desc}</td>
                        <td>{v.video_asgie_title_pt}</td>
                        <td>{v.video_date_creation}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </Column>
        </Row>
      </section>
    );
  }

  /**
  * Filters by video title
  */ 
  _setVideosFilter = data => {
    let result = [];
    let _data = this._filterVideos(data, true);

    _.each(_data, value => {
      result.push({
        value: ` ${value.video_title}`
      });
    });

    return result;
  };

  /**
  * Filters by asgie
  */ 
  _setAsgieFilter = data => {
    let result = [];

    _.each(data, value => {
      result.push({
        value: ` ${value.title_pt}`
      });
    });

    return result;
  };

  /**
  * Filters the videos based on some current filter states
  */ 
  _filterVideos = (data, noTitle) => {
    let { asgieFilter, fromFilter, toFilter, titleFilter } = this.state;
    let values = data;

    if (titleFilter && !noTitle) {
      values = _.filter(values, function(v) {
        return _.trim(v.video_title) == _.trim(titleFilter);
      });
    }

    if (asgieFilter) {
      values = _.filter(values, function(v) {
        return _.trim(v.video_asgie_title_pt) == _.trim(asgieFilter);
      });
    }

    if (fromFilter) {
      let _fromFilter = Moment(fromFilter).format('YYYY-MM-DD');
      values = _.filter(values, function(v) {
        return v.video_date_creation >= _fromFilter;
      });
    }

    if (toFilter) {
      let _toFilter = Moment(toFilter).format('YYYY-MM-DD');
      values = _.filter(values, function(v) {
        return v.video_date_creation <= _toFilter;
      });
    }

    return values;
  };
}

VideosCheck.propTypes = {};

VideosCheck.defaultProps = {};

import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import { Row, Column } from 'react-foundation';
import _ from 'lodash';

import Header from '../Header';
import Sidebar from './Sidebar';
import Breadcrumbs from '../../redux/containers/Breadcrumbs';
import Home from './Home';
import AsgieEdit from '../../redux/containers/AsgieEdit';
import UserAdd from '../../redux/containers/UserAdd';
import UserEdit from '../../redux/containers/UserEdit';
import SourceAdd from '../../redux/containers/SourceAdd';
import SubSourceAdd from '../../redux/containers/SubSourceAdd';
import SourceEdit from '../../redux/containers/SourceEdit';
import VideosCheck from '../../redux/containers/VideosCheck';
import Servers from '../../redux/containers/Servers';
import UsageAnalysis from '../../redux/containers/UsageAnalysis';

import Socket from '../../Socket';

/**
* DashBoard Main component 
* Connects react router and all the components
*/
export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    Socket.connect(`client.1`, `RefreshContent`);
  }

  render() {
    return (
      <Router>
        <div>
          <Header />
          <section className="body">
            <Row className="body__row">
              <Column small={3} medium={2} large={2}>
                <Sidebar />
              </Column>
              <Column small={9} medium={10} large={10}>
                <Breadcrumbs />
                <div className="mainContent">
                  <Route exact path="/" component={Home} />
                  <Route exact path="/sources/add" component={SourceAdd} />
                  <Route
                    exact
                    path="/sources/add/sub"
                    component={SubSourceAdd}
                  />
                  <Route exact path="/sources/edit" component={SourceEdit} />
                  <Route exact path="/asgie/edit" component={AsgieEdit} />
                  <Route exact path="/users/add" component={UserAdd} />
                  <Route exact path="/users/edit" component={UserEdit} />
                  <Route exact path="/videos/check" component={VideosCheck} />
                  <Route
                    exact
                    path="/system_status/servers"
                    component={Servers}
                  />
                  <Route
                    exact
                    path="/system_status/usage_analysis"
                    component={UsageAnalysis}
                  />
                </div>
              </Column>
            </Row>
          </section>
        </div>
      </Router>
    );
  }
}

Dashboard.propTypes = {};
Dashboard.defaultProps = {};

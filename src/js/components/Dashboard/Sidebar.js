import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, MenuItem, Icon } from 'react-foundation';
import _ from 'lodash';


/**
* Sidebar menu with navigation drawers
* Uses React foundation MenuItem to navigate on the app. Each item Uses React-router-dom Link to redirect
*/
export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawers: {
        sources: false,
        asgie: false,
        users: false,
        videos: false,
        systemStatus: false
      }
    };
  }

  render() {
    return (
      <section className="sidebar">
        <div className="sidebar__ul">
          <Menu isVertical>
            <MenuItem>
              <Link to="/">
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-home" />
                  </span>Dashboard
                </div>
              </Link>
            </MenuItem>
            <MenuItem>
              <a onClick={() => this._drawer('sources')}>
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-page-multiple" />
                  </span>Sources
                  <span className="menuItem__expand">
                    <Icon name="fi-plus" />
                  </span>
                </div>
              </a>
              {this.state.drawers.sources && (
                <Menu key="sourcesKey" isVertical isNested>
                  <MenuItem>
                    <Link to="/sources/add">
                      <div className="nested__item">Add</div>
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link to="/sources/add/sub">
                      <div className="nested__item">Add Sub</div>
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link to="/sources/edit">
                      <div className="nested__item">Edit</div>
                    </Link>
                  </MenuItem>
                </Menu>
              )}
            </MenuItem>
            <MenuItem>
              <a onClick={() => this._drawer('asgie')}>
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-torsos-all" />
                  </span>ASGIE
                  <span className="menuItem__expand">
                    <Icon name="fi-plus" />
                  </span>
                </div>
              </a>
              {this.state.drawers.asgie && (
                <Menu key="sourcesKey" isVertical isNested>
                  <MenuItem>
                    <Link to="/asgie/edit">
                      <div className="nested__item">Check</div>
                    </Link>
                  </MenuItem>
                </Menu>
              )}
            </MenuItem>
            <MenuItem>
              <a onClick={() => this._drawer('users')}>
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-torsos" />
                  </span>Users
                  <span className="menuItem__expand">
                    <Icon name="fi-plus" />
                  </span>
                </div>
              </a>
              {this.state.drawers.users && (
                <Menu key="sourcesKey" isVertical isNested>
                  <MenuItem>
                    <Link to="/users/add">
                      <div className="nested__item">Add</div>
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link to="/users/edit">
                      <div className="nested__item">Edit</div>
                    </Link>
                  </MenuItem>
                </Menu>
              )}
            </MenuItem>
            <MenuItem>
              <a onClick={() => this._drawer('videos')}>
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-video" />
                  </span>Videos
                  <span className="menuItem__expand">
                    <Icon name="fi-plus" />
                  </span>
                </div>
              </a>
              {this.state.drawers.videos && (
                <Menu key="sourcesKey" isVertical isNested>
                  <MenuItem>
                    <Link to="/videos/check">
                      <div className="nested__item">Check</div>
                    </Link>
                  </MenuItem>
                </Menu>
              )}
            </MenuItem>
            <MenuItem>
              <a onClick={() => this._drawer('systemStatus')}>
                <div className="menuItem">
                  <span className="menuItem__icon">
                    <Icon name="fi-monitor" />
                  </span>System Status
                  <span className="menuItem__expand">
                    <Icon name="fi-plus" />
                  </span>
                </div>
              </a>
              {this.state.drawers.systemStatus && (
                <Menu key="sourcesKey" isVertical isNested>
                  <MenuItem>
                    <Link to="/system_status/servers">
                      <div className="nested__item">Servers</div>
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link to="/system_status/usage_analysis">
                      <div className="nested__item">Usage analysis</div>
                    </Link>
                  </MenuItem>
                </Menu>
              )}
            </MenuItem>
          </Menu>
        </div>
      </section>
    );
  }

  /**
  * Opens the elements of the navigatiopn drawer
  */
  _drawer = drawer => {
    let drawers = this.state.drawers;
    _.each(this.state.drawers, (v, k) => {
      if (drawer === k) {
        drawers[k] = !v;
      } else {
        drawers[k] = false;
      }
    });
    this.setState({ drawers });
  };
}

Sidebar.propTypes = {};

Sidebar.defaultProps = {};

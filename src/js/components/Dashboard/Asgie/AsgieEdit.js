import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column } from 'react-foundation';

/**
* Asgie Edition
* Component that, in the moment, display the available ASGIE
*/
export default class AsgieEdit extends React.Component {
  constructor(props) {
    super();
  }


 /**
 * Calls the ASGIE data with a HTTP request
 */
  componentWillMount() {
    this.props.actionCreators.asgie();
  }

 /**
 * Updates the received props from redux
 */
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
    console.log(nextProps.actionData.asgie.data);
  }

  render() {
    let asgie = this.props.actionData.asgie.data;
    return (
      <section className="asgieEdit">
        <Row>
          <Column>
            <table>
              <thead>
                <tr>
                  <th width="300">Title</th>
                  <th>Portuguese Title</th>
                  <th>Color</th>
                  <th>Image</th>
                </tr>
              </thead>
              <tbody>
                {asgie.map((v, k) => {
                  return (
                    <tr key={`tr_${k}`}>
                      <td>{v.title}</td>
                      <td>{v.title_pt}</td>
                      <td>{v.color}</td>
                      <td>{v.image}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </Column>
        </Row>
      </section>
    );
  }
}

AsgieEdit.propTypes = {};

AsgieEdit.defaultProps = {};

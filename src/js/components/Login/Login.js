import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Row, Column } from 'react-foundation';
import FormLogin from './Form/FormLogin';
import _ from 'lodash';
import { setToken, setRefreshToken, getRefreshToken } from '../../Cookies';

/**
* Login
* This components is for the Dashboard Login page and connects to a FormLogin component
*/
export default class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
  * Gets the refresh token from a cookie file. src/js/Cookies.js and requests a new refesh token from the API
  */
  componentWillMount() {
    if (!_.isEmpty(getRefreshToken())) {
      this.props.actionCreators.refreshToken(getRefreshToken());
    }
  }

  /**
  * Handles tokens according to login state
  */
  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
    const token = this.props.actionData.login.data;
    const refreshToken = this.props.actionData.refreshToken.data;
    if (!_.isEmpty(token)) {
      setToken(token.access_token, token.expires_in);
      setRefreshToken(token.refresh_token);
    } else if (!_.isEmpty(refreshToken)) {
      setToken(refreshToken.access_token, refreshToken.expires_in);
      setRefreshToken(refreshToken.refresh_token);
    }
  }

  render() {
    return (
      <section className="login">
        <div className="login__space">
          <div className="title-big">
            <span className="blue">+</span>TV<span className="blue">4</span>E |
            Login
          </div>
          <FormLogin onSubmit={this._submit} />
        </div>
      </section>
    );
  }

  /**
  * Makes login http request according to login form data
  */
  _submit = data => {
    this.props.actionCreators.login(data);
  };
}

Login.propTypes = {};

Login.defaultProps = {};

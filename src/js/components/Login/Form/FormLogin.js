import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-foundation';
import {
  required,
  number,
  alphaNumeric,
  maxLength15,
  minLength2,
  RenderField,
  RenderDropdownList
} from '../../Form/Validator';

/**
* Login Form
* Login Form with username and password form fields
*/
let FormLogin = props => {
  const { handleSubmit, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="username"
        component={RenderField}
        type="text"
        label="Username"
        // validate={[required, minLength2]}
        // warn={alphaNumeric}
      />
      <Field
        name="password"
        component={RenderField}
        label="Password"
        type="password"
      />
      <div className="login__space-button">
        <Button type="submit" disabled={submitting}>
          Submit
        </Button>
      </div>
    </form>
  );
};

/**
* It is given a unique identifier name for the form, to connect its components and to identify it within the redux-form action Data
*/
FormLogin = reduxForm({
  // a unique name for the form
  form: 'contact'
})(FormLogin);

export default FormLogin;

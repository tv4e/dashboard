import React from 'react';

/**
* Editable Field
* An editable text field input
*/
export default class EditableField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: false,
      value: ''
    };
  }

  render() {
    let { selected, value } = this.state;
    let { valueUnselected, onSubmit, name } = this.props;

    return (
      <div onDoubleClick={() => this._editField()} className="editableField">
        {selected ? (
          <input
            onBlur={() => {
              this._onBlur();
              onSubmit({ [name]: value });
            }}
            ref={input => {
              this.input = input;
            }}
            type="text"
            value={value}
            onKeyUp={event => this._pressEnter(event)}
            onChange={event => this.setState({ value: event.target.value })}
          />
        ) : (
          valueUnselected
        )}
      </div>
    );
  }

  
  _editField = () => {
    this.setState(
      {
        selected: true,
        value: this.props.valueSelected
      },
      () => {
        this.input.focus();
      }
    );
  };

  /**
   * When the component is blured the Dropdown is unselected
   */
  _onBlur = () => {
    this.setState({ selected: false });
  };

  /**
   * When Enter is pressed the onSubmit callback is called and the input is blurred
   */
  _pressEnter = event => {
    if (event.keyCode === 13) {
      this.input.blur();
      this.props.onSubmit({ [this.props.name]: this.state.value });
    }
  };
}

EditableField.propTypes = {
  /**
   * The input name
   */
  name: React.PropTypes.string.isRequired
  // valueSelected: React.PropTypes.oneOfType([
  //   React.PropTypes.string,
  //   React.PropTypes.integer
  // ]),
};

EditableField.defaultProps = {};

import React from 'react';
import DropdownList from 'react-widgets/lib/DropdownList';

/**
 * Editable DropDown
 * A dropdown that can get text inputs
 */
export default class EditableDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: false,
      value: props.defaultValue,
      open: true
    };
  }

  render() {
    let { selected, value, open } = this.state;
    let {
      valueUnselected,
      data,
      defaultValue,
      filter,
      groupBy,
      valueField,
      textField,
      onSubmit
    } = this.props;

    let ValueInput = ({ item }) => {
      if (typeof item == 'string') {
        return <span>{item}</span>;
      } else {
        let separator =
          item.separator != undefined ? `${item.separator} | ` : '';

        return (
          <span>
            <strong>{separator}</strong>
            {item.value}
          </span>
        );
      }
    };

    let ListItem = ({ item }) => {
      if (typeof item == 'string') {
        return <span>{item}</span>;
      } else {
        return (
          <span>
            <strong>{item.prefix}</strong>
            {' ' + item.value}
          </span>
        );
      }
    };

    return (
      <div onDoubleClick={() => this._editField()} className="editableField">
        {selected ? (
          <DropdownList
            filter={filter}
            data={data}
            valueField={valueField}
            textField={textField}
            valueComponent={ValueInput}
            itemComponent={ListItem}
            defaultValue={defaultValue}
            groupBy={groupBy}
            open={open}
            onChange={value => this.setState({ value, open: !this.state.open })}
            onBlur={() => {
              this._onBlur();
              if (value != undefined) {
                onSubmit(value);
              }
            }}
            onToggle={() => {}}
          />
        ) : (
          valueUnselected
        )}
      </div>
    );
  }

  _editField = () => {
    this.setState({
      selected: true,
      value: this.props.valueSelected,
      open: true
    });
  };


  /**
   * When the component is blured the Dropdown is unselected
   */
  _onBlur = () => {
    this.setState({ selected: false, open: !this.state.open });
  };

  /**
   * When Enter is pressed the onSubmit callback is called and the input is blurred
   */
  _pressEnter = event => {
    if (event.keyCode === 13) {
      this.input.blur();
      this.props.onSubmit({ [this.props.name]: this.state.value });
    }
  };
}

EditableDropdown.propTypes = {
  /**
   * The input name
   */
  name: React.PropTypes.string.isRequired
  // valueSelected: React.PropTypes.oneOfType([
  //   React.PropTypes.string,
  //   React.PropTypes.integer
  // ]),
};

EditableDropdown.defaultProps = {};

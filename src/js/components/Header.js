import React from 'react';
import {
  TopBar,
  TopBarTitle,
  TopBarRight,
  Menu,
  MenuItem
} from 'react-foundation';
import logo from '../../assets/logo/logo.png';

/**
* Header
* Header with logo and loged in user. In this case the user is hard coded
*/
export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="header">
        <TopBar className="navbar">
          <TopBarTitle className="navbar__title">
            <img src={logo} alt="Logo" />
          </TopBarTitle>
          <TopBarRight className="navbar__right">
            <Menu>
              <MenuItem>
                Telmo Silva
                <img
                  className="navbar__userImage"
                  src="http://0.gravatar.com/avatar/a7ab5f9cc97a586bc7ee62ccc7fb5078?s=128&d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D128&r=G"
                  alt="User"
                />
              </MenuItem>
            </Menu>
          </TopBarRight>
        </TopBar>
      </section>
    );
  }
}

Header.propTypes = {};

Header.defaultProps = {};

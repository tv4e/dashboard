import React from 'react';

import Login from './redux/containers/Login';
import Dashboard from './redux/containers/Dashboard';
import { Route } from 'react-router-dom';

export default class Body extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Dashboard />
        <Route path="/login" component={Login} />
      </div>
    );
  }
}

Body.propTypes = {};

Body.defaultProps = {};

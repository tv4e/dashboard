export const getToken = () => {
  return getCookie('token');
};

export const setToken = (token, expires) => {
  let duration = new Date();
  duration.setSeconds(duration.getSeconds() + expires);
  document.cookie = `token=${token};expires=${expires};path=/`;
};

export const getRefreshToken = () => {
  return getCookie('refreshToken');
};

export const setRefreshToken = refreshToken => {
  document.cookie = `refreshToken=${refreshToken};path=/`;
};

export const logout = () => {
  document.cookie = `token=;path=/`;
  document.cookie = `refreshToken=;path=/`;
};

export const loggedIn = () => {
  if (!!getCookie('token') && !!getCookie('refreshToken')) {
    return true;
  } else {
    return false;
  }
};

const getCookie = cname => {
  var name = cname + '=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
};

export function loginSuccess(state = [], action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function loginIsLoading(state = false, action) {
  switch (action.type) {
    case 'LOGIN_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function loginHasErrored(state = false, action) {
  switch (action.type) {
    case 'LOGIN_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//....................REFRESH_TOKEN....................

export function refreshTokenSuccess(state = [], action) {
  switch (action.type) {
    case 'REFRESH_TOKEN_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function refreshTokenIsLoading(state = false, action) {
  switch (action.type) {
    case 'REFRESH_TOKEN_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function refreshTokenHasErrored(state = false, action) {
  switch (action.type) {
    case 'REFRESH_TOKEN_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

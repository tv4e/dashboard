export function boxesSuccess(state = [], action) {
  switch (action.type) {
    case 'BOXES_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function boxesIsLoading(state = false, action) {
  switch (action.type) {
    case 'BOXES_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function boxesHasErrored(state = false, action) {
  switch (action.type) {
    case 'BOXES_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

export function boxStateSuccess(state = [], action) {
  switch (action.type) {
    case 'BOX_STATE_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function boxStateIsLoading(state = false, action) {
  switch (action.type) {
    case 'BOX_STATE_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function boxStateHasErrored(state = false, action) {
  switch (action.type) {
    case 'BOX_STATE_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................DISTRICTS.............................................

export function districtsSuccess(state = [], action) {
  switch (action.type) {
    case 'DISTRICTS_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function districtsIsLoading(state = false, action) {
  switch (action.type) {
    case 'DISTRICTS_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function districtsHasErrored(state = false, action) {
  switch (action.type) {
    case 'DISTRICTS_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................BOXES POST.............................................

export function postBoxSuccess(state = [], action) {
  switch (action.type) {
    case 'POST_BOX_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function postBoxIsLoading(state = false, action) {
  switch (action.type) {
    case 'POST_BOX_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function postBoxHasErrored(state = false, action) {
  switch (action.type) {
    case 'POST_BOX_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

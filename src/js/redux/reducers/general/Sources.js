export function asgieSuccess(state = [], action) {
  switch (action.type) {
    case 'ASGIE_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function asgieIsLoading(state = false, action) {
  switch (action.type) {
    case 'ASGIE_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function asgieHasErrored(state = false, action) {
  switch (action.type) {
    case 'ASGIE_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................DISTRICTS.............................................

export function districtsSuccess(state = [], action) {
  switch (action.type) {
    case 'DISTRICTS_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function districtsIsLoading(state = false, action) {
  switch (action.type) {
    case 'DISTRICTS_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function districtsHasErrored(state = false, action) {
  switch (action.type) {
    case 'DISTRICTS_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................SOURCES.............................................

export function sourcesSuccess(state = [], action) {
  switch (action.type) {
    case 'SOURCES_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function sourcesIsLoading(state = false, action) {
  switch (action.type) {
    case 'SOURCES_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function sourcesHasErrored(state = false, action) {
  switch (action.type) {
    case 'SOURCES_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................SOCIAL.............................................

export function socialSuccess(state = [], action) {
  switch (action.type) {
    case 'SOCIAL_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function socialIsLoading(state = false, action) {
  switch (action.type) {
    case 'SOCIAL_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function socialHasErrored(state = false, action) {
  switch (action.type) {
    case 'SOCIAL_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................POST SOURCES.............................................

export function postSourcesSuccess(state = [], action) {
  switch (action.type) {
    case 'POST_SOURCES_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function postSourcesIsLoading(state = false, action) {
  switch (action.type) {
    case 'POST_SOURCES_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function postSourcesHasErrored(state = false, action) {
  switch (action.type) {
    case 'POST_SOURCES_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//.....................POST SUB SOURCES.............................................

export function postSubSourcesSuccess(state = [], action) {
  switch (action.type) {
    case 'POST_SUB_SOURCES_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function postSubSourcesIsLoading(state = false, action) {
  switch (action.type) {
    case 'POST_SUB_SOURCES_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function postSubSourcesHasErrored(state = false, action) {
  switch (action.type) {
    case 'POST_SUB_SOURCES_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

export function jenkinsVideoCronSuccess(state = [], action) {
  switch (action.type) {
    case 'JENKINS_VIDEO_CRON_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function jenkinsVideoCronIsLoading(state = false, action) {
  switch (action.type) {
    case 'JENKINS_VIDEO_CRON_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function jenkinsVideoCronHasErrored(state = false, action) {
  switch (action.type) {
    case 'JENKINS_VIDEO_CRON_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}
//..................................jenkinsDelete0........................

export function jenkinsDeleteCronSuccess(state = [], action) {
  switch (action.type) {
    case 'JENKINS_DELETE_CRON_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function jenkinsDeleteCronIsLoading(state = false, action) {
  switch (action.type) {
    case 'JENKINS_DELETE_CRON_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function jenkinsDeleteCronHasErrored(state = false, action) {
  switch (action.type) {
    case 'JENKINS_DELETE_CRON_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}
//..................................jenkinsServer0........................

export function jenkinsWebSuccess(state = [], action) {
  switch (action.type) {
    case 'JENKINS_WEB_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function jenkinsWebHasErrored(state = false, action) {
  switch (action.type) {
    case 'JENKINS_WEB_HAS_ERRORED':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function jenkinsWebIsLoading(state = false, action) {
  switch (action.type) {
    case 'JENKINS_WEB_IS_LOADING':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

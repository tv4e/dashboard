export function rewatchedLibrarySuccess(state = [], action) {
  switch (action.type) {
    case 'REWATCHED_LIBRARY_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function rewatchedLibraryIsLoading(state = false, action) {
  switch (action.type) {
    case 'REWATCHED_LIBRARY_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function rewatchedLibraryHasErrored(state = false, action) {
  switch (action.type) {
    case 'REWATCHED_LIBRARY_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//........................SEEN VIDEOS

export function seenSuccess(state = [], action) {
  switch (action.type) {
    case 'SEEN_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function seenIsLoading(state = false, action) {
  switch (action.type) {
    case 'SEEN_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function seenHasErrored(state = false, action) {
  switch (action.type) {
    case 'SEEN_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

//........................UNSEEN VIDEOS

export function unseenSuccess(state = [], action) {
  switch (action.type) {
    case 'UNSEEN_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function unseenIsLoading(state = false, action) {
  switch (action.type) {
    case 'UNSEEN_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function unseenHasErrored(state = false, action) {
  switch (action.type) {
    case 'UNSEEN_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

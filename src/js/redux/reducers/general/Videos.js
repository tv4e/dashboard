export function videosSuccess(state = [], action) {
  switch (action.type) {
    case 'VIDEOS_SUCCESS':
      return action.response;
      break;
    default:
      return state;
  }
}

export function videosIsLoading(state = false, action) {
  switch (action.type) {
    case 'VIDEOS_IS_LOADING':
      return action.isLoading;
      break;
    default:
      return state;
  }
}

export function videosHasErrored(state = false, action) {
  switch (action.type) {
    case 'VIDEOS_HAS_ERRORED':
      return action.hasErrored;
      break;
    default:
      return state;
  }
}

import _ from 'lodash';
import { reducer as formReducer } from 'redux-form';
import * as Jenkins from './general/Jenkins';
import * as Profile from './general/Profile';
import * as Boxes from './general/Boxes';
import * as Sources from './general/Sources';
import * as Socket from './general/Socket';
import * as Videos from './general/Videos';
import * as UsageAnalysis from './general/Statistics';

const reducers = _.extend(
  {},
  Profile,
  Boxes,
  Socket,
  Sources,
  Jenkins,
  UsageAnalysis,
  Videos,
  {
    form: formReducer
  }
);

export default reducers;

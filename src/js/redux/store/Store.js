import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as allReducers from '../reducers/Reducers';
import thunk from 'redux-thunk';

const initialState = {};

const reducers = combineReducers(allReducers.default);

const store = createStore(reducers, initialState, applyMiddleware(thunk));

// if (module.hot) {
//     // Enable Webpack hot module replacement for reducers
//     module.hot.accept('../reducers/reducers', () => {
//         const reducers = combineReducers(allReducers);
//         store.replaceReducer(reducers);
//     });
// }

export default store;

import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import UserAdd from '../../components/Dashboard/Users/Add/UserAdd';
import * as Boxes from './../actions/Boxes';
import * as Sources from './../actions/Sources';

const mapStateToProps = function(state) {
  return {
    actionData: {
      postBox: {
        data: state.postBoxSuccess,
        hasErrored: state.postBoxHasErrored,
        isLoading: state.postBoxIsLoading
      },
      districts: {
        data: state.districtsSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Boxes, Sources);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserAdd)
);

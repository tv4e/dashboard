import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import UsageAnalysis from '../../components/Dashboard/SystemStatus/UsageAnalysis';
import * as Boxes from '../actions/Boxes';
import * as Statistics from '../actions/Statistics';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  return {
    actionData: {
      rewatchedLibrary: {
        data: state.rewatchedLibrarySuccess,
        hasErrored: state.rewatchedLibraryHasErrored,
        isLoading: state.rewatchedLibraryIsLoading
      },
      seen: {
        data: state.seenSuccess,
        hasErrored: state.seenHasErrored,
        isLoading: state.seenIsLoading
      },
      unseen: {
        data: state.unseenSuccess,
        hasErrored: state.unseenHasErrored,
        isLoading: state.unseenIsLoading
      },
      boxes: {
        data: state.boxesSuccess,
        hasErrored: state.boxesHasErrored,
        isLoading: state.boxesIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Statistics, Boxes, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UsageAnalysis)
);

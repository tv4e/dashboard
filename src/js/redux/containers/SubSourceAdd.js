import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import SubSourceAdd from '../../components/Dashboard/Sources/AddSub/SubSourceAdd';
import * as Sources from './../actions/Sources';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  return {
    actionData: {
      asgie: {
        data: state.asgieSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      },
      districts: {
        data: state.districtsSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      },
      social: {
        data: state.socialSuccess,
        hasErrored: state.socialHasErrored,
        isLoading: state.socialIsLoading
      },
      sources: {
        data: state.sourcesSuccess,
        hasErrored: state.sourcesHasErrored,
        isLoading: state.sourcesIsLoading
      },
      postSubSources: {
        data: state.postSubSourcesSuccess,
        hasErrored: state.postSubSourcesHasErrored,
        isLoading: state.postSubSourcesIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Sources, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SubSourceAdd)
);

import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import Servers from '../../components/Dashboard/SystemStatus/Servers';
import * as Jenkins from './../actions/Jenkins';

const mapStateToProps = function(state) {
  return {
    actionData: {
      jenkinsVideoCron: {
        data: state.jenkinsVideoCronSuccess,
        hasErrored: state.jenkinsVideoCronHasErrored,
        isLoading: state.jenkinsVideoCronIsLoading
      },
      jenkinsDeleteCron: {
        data: state.jenkinsDeleteCronSuccess,
        hasErrored: state.jenkinsDeleteCronHasErrored,
        isLoading: state.jenkinsDeleteCronIsLoading
      },
      jenkinsWeb: {
        data: state.jenkinsWebSuccess,
        hasErrored: state.jenkinsWebHasErrored,
        isLoading: state.jenkinsWebIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Jenkins);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Servers)
);

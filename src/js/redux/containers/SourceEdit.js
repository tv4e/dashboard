import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import SourceEdit from '../../components/Dashboard/Sources/SourceEdit';
import * as Sources from './../actions/Sources';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  return {
    actionData: {
      sources: {
        data: state.sourcesSuccess,
        hasErrored: state.sourcesHasErrored,
        isLoading: state.sourcesIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Sources, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SourceEdit)
);

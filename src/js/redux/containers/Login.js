import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import { userHasAuthenticated } from './../../Auth';
import Login from './../../components/Login/Login';
import * as Profile from '../actions/Profile';

const mapStateToProps = function(state) {
  return {
    actionData: {
      login: {
        data: state.loginSuccess,
        hasErrored: state.loginHasErrored,
        isLoading: state.loginIsLoading
      },
      refreshToken: {
        data: state.refreshTokenSuccess,
        hasErrored: state.refreshTokenHasErrored,
        isLoading: state.refreshTokenIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Profile);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(userHasAuthenticated(Login))
);

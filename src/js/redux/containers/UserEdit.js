import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import UserEdit from '../../components/Dashboard/Users/UserEdit';
import * as Boxes from './../actions/Boxes';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  let boxes =
    state.socket.RefreshContent != undefined
      ? _.merge(state.boxesSuccess, state.socket.RefreshContent.data.original)
      : state.boxesSuccess;

  if (state.socket.RefreshContent != undefined) {
    console.log(state.socket.RefreshContent.data.original);
  }

  return {
    actionData: {
      boxes: {
        data: boxes,
        hasErrored: state.boxesHasErrored,
        isLoading: state.boxesIsLoading
      },
      districts: {
        data: state.districtsSuccess,
        hasErrored: state.districtsHasErrored,
        isLoading: state.districtsIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Boxes, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserEdit)
);

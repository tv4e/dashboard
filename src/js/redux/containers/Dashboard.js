import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import Dashboard from './../../components/Dashboard/Dashboard';
import { isUserAuthenticated } from './../../Auth';

const mapStateToProps = function(state) {
  return {
    actionData: {}
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({});

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(isUserAuthenticated(Dashboard))
);

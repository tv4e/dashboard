import Breadcrumbs from './../../components/Breadcrumbs';
import { withRouter } from 'react-router-dom';

export default withRouter(Breadcrumbs);

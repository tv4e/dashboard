import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import SourceAdd from '../../components/Dashboard/Sources/Add/SourceAdd';
import * as Sources from './../actions/Sources';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  return {
    actionData: {
      asgie: {
        data: state.asgieSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      },
      districts: {
        data: state.districtsSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      },
      postSources: {
        data: state.postSourcesSuccess,
        hasErrored: state.postSourcesHasErrored,
        isLoading: state.postSourcesIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Sources, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SourceAdd)
);

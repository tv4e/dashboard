import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import Body from './../../Body';

const mapStateToProps = function(state) {
  return {};
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, {});

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Body));

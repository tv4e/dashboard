import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import VideosCheck from '../../components/Dashboard/Videos/VideosCheck';
import * as Videos from './../actions/Videos';
import * as Socket from './../actions/Socket';
import * as Sources from './../actions/Sources';

const mapStateToProps = function(state) {
  return {
    actionData: {
      asgie: {
        data: state.asgieSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      },
      videos: {
        data: state.videosSuccess,
        hasErrored: state.videosHasErrored,
        isLoading: state.videosIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Videos, Sources, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(VideosCheck)
);

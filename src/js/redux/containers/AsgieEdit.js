import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import AsgieEdit from '../../components/Dashboard/Asgie/AsgieEdit';
import * as Sources from './../actions/Sources';
import * as Socket from './../actions/Socket';

const mapStateToProps = function(state) {
  return {
    actionData: {
      asgie: {
        data: state.asgieSuccess,
        hasErrored: state.asgieHasErrored,
        isLoading: state.asgieIsLoading
      }
    }
  };
};

const mapDispatchToProps = function(dispatch) {
  let Actions = _.extend({}, Sources, Socket);

  return { actionCreators: bindActionCreators(Actions, dispatch) };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AsgieEdit)
);

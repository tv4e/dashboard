import query_string from 'query-string';
import axios from 'axios';

//ACTION CREATORS
export function videosSuccess(response) {
  return {
    type: 'VIDEOS_SUCCESS',
    response
  };
}

export function videosHasErrored(bool) {
  return {
    type: 'VIDEOS_HAS_ERRORED',
    hasErrored: bool
  };
}

export function videosIsLoading(bool) {
  return {
    type: 'VIDEOS_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function videos() {
  return dispatch => {
    dispatch(videosIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/statistics/videos`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(videosIsLoading(false));
        dispatch(videosSuccess(response.data));
      })
      .catch(() => dispatch(videosHasErrored(true)));
  };
}

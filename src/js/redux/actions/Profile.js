import axios from 'axios';
import query_string from 'query-string';

//ACTION CREATORS
export function loginSuccess(response) {
  return {
    type: 'LOGIN_SUCCESS',
    response
  };
}

export function loginHasErrored(bool) {
  return {
    type: 'LOGIN_HAS_ERRORED',
    hasErrored: bool
  };
}

export function loginIsLoading(bool) {
  return {
    type: 'LOGIN_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function login(data) {
  let payload = {
    grant_type: 'password',
    client_id: 2,
    client_secret: '2swKu8IxdF2BrTKXJUamnotBuC1dda8c1pcoZOVU',
    username: data.username,
    password: data.password
  };

  return dispatch => {
    dispatch(loginIsLoading(true));
    axios({
      method: 'post',
      url: 'http://api_mysql.tv4e.pt/oauth/token',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded'
      },
      data: query_string.stringify(payload)
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response);
        }
        response = response.data;
        dispatch(loginIsLoading(false));
        dispatch(loginSuccess(response));
      })
      .catch(error => {
        if (error.response) {
          // errors(error.response.status);
        }
        console.log('error', error.statusText);
        dispatch(loginHasErrored(true));
      });
  };
}

//ACTION CREATORS
export function refreshTokenSuccess(response) {
  return {
    type: 'REFRESH_TOKEN_SUCCESS',
    response
  };
}

export function refreshTokenHasErrored(bool) {
  return {
    type: 'REFRESH_TOKEN_ERRORED',
    hasErrored: bool
  };
}

export function refreshTokenIsLoading(bool) {
  return {
    type: 'REFRESH_TOKEN_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function refreshToken(refreshToken) {
  let payload = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
    client_id: 2,
    client_secret: '2swKu8IxdF2BrTKXJUamnotBuC1dda8c1pcoZOVU'
  };

  return dispatch => {
    dispatch(refreshTokenIsLoading(true));
    axios({
      method: 'post',
      url: 'http://api_mysql.tv4e.pt/oauth/token',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded'
      },
      data: query_string.stringify(payload)
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response);
        }
        response = response.data;
        dispatch(refreshTokenIsLoading(false));
        dispatch(refreshTokenSuccess(response));
      })
      .catch(error => {
        if (error.response) {
          // errors(error.response.status);
        }
        console.log('error', error.statusText);
        dispatch(refreshTokenHasErrored(true));
      });
  };
}

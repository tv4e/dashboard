import query_string from 'query-string';
import axios from 'axios';

//JENKINS CREDENTIALS
// const getCrumb = callback => {
//   axios({
//     method: 'get',
//     url: `http://jenkins1.tv4e.pt/crumbIssuer/api/json`,
//     header: {
//       'Access-Control-Allow-Origin': 'http://localhost:3000',
//       'Access-Control-Allow-Method': 'GET, POST, OPTIONS',
//       'Access-Control-Allow-Credentials': 'true'
//     },
//     auth: {
//       username: '+tv4e',
//       password: '422fca1dbb0c239a3b8f203d1a08315f'
//     }
//   })
//     .then(response => {
//       if (response.status != 200) {
//         return 'error';
//         throw Error(response.statusText);
//       }
//
//       callback(response.data);
//     })
//     .catch(() => {
//       callback('error');
//     });
// };

//ACTION CREATORS
export function jenkinsVideoCronSuccess(response) {
  return {
    type: 'JENKINS_VIDEO_CRON_SUCCESS',
    response
  };
}

export function jenkinsVideoCronHasErrored(bool) {
  return {
    type: 'JENKINS_VIDEO_CRON_HAS_ERRORED',
    hasErrored: bool
  };
}

export function jenkinsVideoCronIsLoading(bool) {
  return {
    type: 'JENKINS_VIDEO_CRON_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function jenkinsVideoCron() {
  return dispatch => {
    dispatch(jenkinsVideoCronIsLoading(true));
    axios({
      method: 'get',
      url: `http://jenkins1.tv4e.pt/job/video-cron/api/json?pretty=true`,
      auth: {
        username: '+tv4e',
        password: '422fca1dbb0c239a3b8f203d1a08315f'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(jenkinsVideoCronIsLoading(false));
        dispatch(jenkinsVideoCronSuccess(response.data));
      })
      .catch(() => dispatch(jenkinsVideoCronHasErrored(true)));
  };
}
//..................................jenkinsDelete0........................

//ACTION CREATORS
export function jenkinsDeleteCronSuccess(response) {
  return {
    type: 'JENKINS_DELETE_CRON_SUCCESS',
    response
  };
}

export function jenkinsDeleteCronHasErrored(bool) {
  return {
    type: 'JENKINS_DELETE_CRON_HAS_ERRORED',
    hasErrored: bool
  };
}

export function jenkinsDeleteCronIsLoading(bool) {
  return {
    type: 'JENKINS_DELETE_CRON_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function jenkinsDeleteCron() {
  return dispatch => {
    dispatch(jenkinsDeleteCronIsLoading(true));
    axios({
      method: 'get',
      url: `http://jenkins1.tv4e.pt/job/cron-delete-video/api/json?pretty=true`,
      auth: {
        username: '+tv4e',
        password: '422fca1dbb0c239a3b8f203d1a08315f'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(jenkinsDeleteCronIsLoading(false));
        dispatch(jenkinsDeleteCronSuccess(response.data));
      })
      .catch(() => dispatch(jenkinsDeleteCronHasErrored(true)));
  };
}

//..................................jenkinsServer0........................

//ACTION CREATORS
export function jenkinsWebSuccess(response) {
  return {
    type: 'JENKINS_WEB_SUCCESS',
    response
  };
}

export function jenkinsWebHasErrored(bool) {
  return {
    type: 'JENKINS_WEB_HAS_ERRORED',
    hasErrored: bool
  };
}

export function jenkinsWebIsLoading(bool) {
  return {
    type: 'JENKINS_WEB_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function jenkinsWeb() {
  return dispatch => {
    dispatch(jenkinsWebIsLoading(true));
    axios({
      method: 'get',
      url: `http://jenkins0.tv4e.pt/job/app.tv4e.pt/api/json?pretty=true`,
      auth: {
        username: '+tv4e',
        password: 'bd808b67ffb1d3c77c87e491bba29d0a'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(jenkinsWebIsLoading(false));
        dispatch(jenkinsWebSuccess(response.data));
      })
      .catch(() => dispatch(jenkinsWebHasErrored(true)));
  };
}

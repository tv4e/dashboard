import query_string from 'query-string';
import axios from 'axios';

//ACTION CREATORS
export function rewatchedLibrarySuccess(response) {
  return {
    type: 'REWATCHED_LIBRARY_SUCCESS',
    response
  };
}

export function rewatchedLibraryHasErrored(bool) {
  return {
    type: 'REWATCHED_LIBRARY_HAS_ERRORED',
    hasErrored: bool
  };
}

export function rewatchedLibraryIsLoading(bool) {
  return {
    type: 'REWATCHED_LIBRARY_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function rewatchedLibrary(payload) {
  return dispatch => {
    dispatch(rewatchedLibraryIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/statistics/videos/box/rewatched`,
      params: payload
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(rewatchedLibraryIsLoading(false));
        dispatch(rewatchedLibrarySuccess(response.data));
      })
      .catch(() => dispatch(rewatchedLibraryHasErrored(true)));
  };
}

//........................SEEN VIDEOS

//ACTION CREATORS
export function seenSuccess(response) {
  return {
    type: 'SEEN_SUCCESS',
    response
  };
}

export function seenHasErrored(bool) {
  return {
    type: 'SEEN_HAS_ERRORED',
    hasErrored: bool
  };
}

export function seenIsLoading(bool) {
  return {
    type: 'SEEN_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function seen(payload) {
  return dispatch => {
    dispatch(seenIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/statistics/videos/box/seen`,
      params: payload
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(seenIsLoading(false));
        dispatch(seenSuccess(response.data));
      })
      .catch(() => dispatch(seenHasErrored(true)));
  };
}

//........................UNSEEN VIDEOS

//ACTION CREATORS
export function unseenSuccess(response) {
  return {
    type: 'UNSEEN_SUCCESS',
    response
  };
}

export function unseenHasErrored(bool) {
  return {
    type: 'UNSEEN_HAS_ERRORED',
    hasErrored: bool
  };
}

export function unseenIsLoading(bool) {
  return {
    type: 'UNSEEN_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function unseen(payload) {
  return dispatch => {
    dispatch(unseenIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/statistics/videos/box/unseen`,
      params: payload
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }
        dispatch(unseenIsLoading(false));
        dispatch(unseenSuccess(response.data));
      })
      .catch(() => dispatch(unseenHasErrored(true)));
  };
}

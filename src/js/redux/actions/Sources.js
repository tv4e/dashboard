import query_string from 'query-string';
import axios from 'axios';
import { getToken } from '../../Cookies';

//ACTION CREATORS
export function asgieSuccess(response) {
  return {
    type: 'ASGIE_SUCCESS',
    response
  };
}

export function asgieHasErrored(bool) {
  return {
    type: 'ASGIE_HAS_ERRORED',
    hasErrored: bool
  };
}

export function asgieIsLoading(bool) {
  return {
    type: 'ASGIE_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function asgie() {
  return dispatch => {
    dispatch(asgieIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/asgies`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(asgieIsLoading(false));
        dispatch(asgieSuccess(response.data));
      })
      .catch(() => dispatch(asgieHasErrored(true)));
  };
}

//.....................DISTRICTS.............................................

//ACTION CREATORS
export function districtsSuccess(response) {
  return {
    type: 'DISTRICTS_SUCCESS',
    response
  };
}

export function districtsHasErrored(bool) {
  return {
    type: 'DISTRICTS_HAS_ERRORED',
    hasErrored: bool
  };
}

export function districtsIsLoading(bool) {
  return {
    type: 'DISTRICTS_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function districts() {
  return dispatch => {
    dispatch(districtsIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/districts`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(districtsIsLoading(false));
        dispatch(districtsSuccess(response.data));
      })
      .catch(() => dispatch(districtsHasErrored(true)));
  };
}

//.............................informationSources............................

//ACTION CREATORS
export function sourcesSuccess(response) {
  return {
    type: 'SOURCES_SUCCESS',
    response
  };
}

export function sourcesHasErrored(bool) {
  return {
    type: 'SOURCES_HAS_ERRORED',
    hasErrored: bool
  };
}

export function sourcesIsLoading(bool) {
  return {
    type: 'SOURCES_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function sources() {
  return dispatch => {
    dispatch(sourcesIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/sources`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(sourcesIsLoading(false));
        dispatch(sourcesSuccess(response.data));
      })
      .catch(() => dispatch(sourcesHasErrored(true)));
  };
}

//.............................cartaSocial............................

//ACTION CREATORS
export function socialSuccess(response) {
  return {
    type: 'SOCIAL_SUCCESS',
    response
  };
}

export function socialHasErrored(bool) {
  return {
    type: 'SOCIAL_HAS_ERRORED',
    hasErrored: bool
  };
}

export function socialIsLoading(bool) {
  return {
    type: 'SOCIAL_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function social() {
  return dispatch => {
    dispatch(socialIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/sources/social`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(socialIsLoading(false));
        dispatch(socialSuccess(response.data));
      })
      .catch(() => dispatch(socialHasErrored(true)));
  };
}

//........................POST informationSources............................

//ACTION CREATORS
export function postSourcesSuccess(response) {
  return {
    type: 'POST_SOURCES_SUCCESS',
    response
  };
}

export function postSourcesHasErrored(bool) {
  return {
    type: 'POST_SOURCES_HAS_ERRORED',
    hasErrored: bool
  };
}

export function postSourcesIsLoading(bool) {
  return {
    type: 'POST_SOURCES_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function postSources(data) {
  return dispatch => {
    dispatch(postSourcesIsLoading(true));
    axios({
      method: 'post',
      url: 'http://api_mysql.tv4e.pt/api/sources',
      headers: {
        Authorization: 'Bearer ' + getToken,
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: data
    })
      .then(response => {
        if (response.status != 200) {
          throw Error(response.statusText);
        }

        dispatch(postSourcesIsLoading(false));
        dispatch(postSourcesSuccess(response.data));
        return response;
      })
      .catch(error => {
        if (error.response) {
        }
        dispatch(postSourcesHasErrored(true));
      });
  };
}
//........................POST informationSubSources............................

//ACTION CREATORS
export function postSubSourcesSuccess(response) {
  return {
    type: 'POST_SUB_SOURCES_SUCCESS',
    response
  };
}

export function postSubSourcesHasErrored(bool) {
  return {
    type: 'POST_SUB_SOURCES_HAS_ERRORED',
    hasErrored: bool
  };
}

export function postSubSourcesIsLoading(bool) {
  return {
    type: 'POST_SUB_SOURCES_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function postSubSources(data) {
  return dispatch => {
    dispatch(postSubSourcesIsLoading(true));
    axios({
      method: 'post8',
      url: 'http://api_mysql.tv4e.pt/api/sources/sub',
      headers: {
        Authorization: 'Bearer ' + getToken,
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: data
    })
      .then(response => {
        if (response.status != 200) {
          throw Error(response.statusText);
        }

        dispatch(postSubSourcesIsLoading(false));
        dispatch(postSubSourcesSuccess(response.data));
        return response;
      })
      .catch(error => {
        if (error.response) {
        }
        dispatch(postSubSourcesHasErrored(true));
      });
  };
}

import store from './../store/Store';
import axios from 'axios';
import query_string from 'query-string';

//ACTION CREATORS
export function socket(response, listener) {
  response.listener = listener;
  return {
    type: 'SOCKET',
    response
  };
}

//ACTION CALLS
export function socketListener(data, listener) {
  store.dispatch(socket(data, listener));
}

import query_string from 'query-string';
import axios from 'axios';

//ACTION CREATORS
export function boxesSuccess(response) {
  return {
    type: 'BOXES_SUCCESS',
    response
  };
}

export function boxesHasErrored(bool) {
  return {
    type: 'BOXES_HAS_ERRORED',
    hasErrored: bool
  };
}

export function boxesIsLoading(bool) {
  return {
    type: 'BOXES_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function boxes() {
  return dispatch => {
    dispatch(boxesIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/boxes`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(boxesIsLoading(false));
        dispatch(boxesSuccess(response.data));
      })
      .catch(() => dispatch(boxesHasErrored(true)));
  };
}

//ACTION CREATORS
export function boxStateSuccess(response) {
  return {
    type: 'BOX_STATE_SUCCESS',
    response
  };
}

export function boxStateHasErrored(bool) {
  return {
    type: 'BOX_STATE_HAS_ERRORED',
    hasErrored: bool
  };
}

export function boxStateIsLoading(bool) {
  return {
    type: 'BOX_STATE_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function boxState(payload, id) {
  return dispatch => {
    dispatch(boxStateIsLoading(true));
    axios({
      method: 'put',
      url: `http://api_mysql.tv4e.pt/api/boxes/box/${id}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: query_string.stringify(payload)
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(boxStateIsLoading(false));
        dispatch(boxStateSuccess(response.data));
      })
      .catch(() => dispatch(boxStateHasErrored(true)));
  };
}

//.....................DISTRICTS.............................................

//ACTION CREATORS
export function districtsSuccess(response) {
  return {
    type: 'DISTRICTS_SUCCESS',
    response
  };
}

export function districtsHasErrored(bool) {
  return {
    type: 'DISTRICTS_HAS_ERRORED',
    hasErrored: bool
  };
}

export function districtsIsLoading(bool) {
  return {
    type: 'DISTRICTS_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function districts() {
  return dispatch => {
    dispatch(districtsIsLoading(true));
    axios({
      method: 'get',
      url: `http://api_mysql.tv4e.pt/api/districts`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(districtsIsLoading(false));
        dispatch(districtsSuccess(response.data));
      })
      .catch(() => dispatch(districtsHasErrored(true)));
  };
}

//.....................BOXES POST.............................................

//ACTION CREATORS
export function postBoxSuccess(response) {
  return {
    type: 'POST_BOX_SUCCESS',
    response
  };
}

export function postBoxHasErrored(bool) {
  return {
    type: 'POST_BOX_HAS_ERRORED',
    hasErrored: bool
  };
}

export function postBoxIsLoading(bool) {
  return {
    type: 'POST_BOX_IS_LOADING',
    isLoading: bool
  };
}

//ACTION CALLS
export function postBox(payload, id) {
  return dispatch => {
    dispatch(postBoxIsLoading(true));
    axios({
      method: 'post',
      url: `http://api_mysql.tv4e.pt/api/boxes/box`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: query_string.stringify(payload)
    })
      .then(response => {
        if (response.status != 200) {
          console.log('error');
          throw Error(response.statusText);
        }

        dispatch(postBoxIsLoading(false));
        dispatch(postBoxSuccess(response.data));
      })
      .catch(() => dispatch(postBoxHasErrored(true)));
  };
}

import React, { Component } from 'react';
import './styles/App/index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Body from './js/redux/containers/Body';
import Store from './js/redux/store/Store';

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <Router>
          <section className="App">
            <Body />
          </section>
        </Router>
      </Provider>
    );
  }
}

export default App;

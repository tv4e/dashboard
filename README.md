# Dashboard

Since the development of all key components of +TV4E project were finished, it was necessary to create a simple way for the administrators to adjust the various elements and settings of the platform. With this mindset a dashboard was created which allowed the adminastration of users, ASGIE, videos and information sources using only a single interface.
Previously adding new information sources was a process that involved a lot of trial and error since it was necessary to experiment with the HTML tags of the information source’s website to find the correct inputs that would generate the videos. The dashboard streamlined this process, which are now added using a web form which can be validated before submitting to ensure that all the information was imputed correct, therefore removing the need to constantly execute the video builder to check if the videos were being generated properly.
The dashboard is also a data analysis tool aiming to provide the research team with quality control strategies over the created informative videos and to select reliable sources of information for each user. It shows data of which videos were created per each ASGIE, as well as data regarding which videos users watched and how they watched them, either via notification, injection or video library. Aditionally the dashboard can provide details regarding the most popular videos and the most frequent watching hours, which can be valuable information to understand which are the most appropriate times to send informative videos.
The dashboard was developed using ReactJS, the same framework used for the ITV application and communicated with the database throught the API.

# Components

### Breadcrumbs
`src/js/components/Breadcrumbs.js`

**Breadcrumbs** Used to locate the current page based on the route
### Asgie Edition
`src/js/components/Dashboard/Asgie/AsgieEdit.js`

**AsgieEdit** Component that, in the moment, display the available ASGIE
### DashBoard Main component 
`src/js/components/Dashboard/Dashboard.js`

**Dashboard** Connects react router and all the components
### Home Screen
`src/js/components/Dashboard/Home.js`

**Home** Initial screen shown when loggin in to the dashboard
### Sidebar menu with navigation drawers
`src/js/components/Dashboard/Sidebar.js`

**Sidebar** Uses React foundation MenuItem to navigate on the app. Each item Uses React-router-dom Link to redirect
### Source Add - Source section
`src/js/components/Dashboard/Sources/Add/Form/FormSourceAdd.js`

**FormSourceAdd** Form fields to add a source, a SubSource (not required), asgie and a city
### Source Add - News links semantics section
`src/js/components/Dashboard/Sources/Add/Form/FormSourceAddLinks.js`

**FormSourceAddLinks** Form fields to add the semantic structure to identify links
### Source Add - News
`src/js/components/Dashboard/Sources/Add/Form/FormSourceAddNews.js`

**FormSourceAddNews** Form fields to add the semantic structure to identify the news title and description
### Sub Source Add - Source section
`src/js/components/Dashboard/Sources/AddSub/Form/FormSubSourceAdd.js`

**FormSubSourceAdd** Form fields to add a source a SubSource, asgie and carta social (not required)
### Sub Source Add - City section
`src/js/components/Dashboard/Sources/AddSub/Form/FormSubSourceAddCity.js`

**FormSubSourceAddCity** Form fields to add a city to a SubSource
### Sub Source Add - Semantic section
`src/js/components/Dashboard/Sources/AddSub/Form/FormSubSourceAddSocial.js`

**FormSubSourceAddSocial** Form fields to add the semantic structure for the webscrapper
### Source edition - Sub Sources
`src/js/components/Dashboard/Sources/ExpandSourceEdit.js`

**ExpandSourceEdit** This section lists all the sub sources in a table. All the elements can be edited and filtered.
### Source edition - Sources
`src/js/components/Dashboard/Sources/NormalSourceEdit.js`

**NormalSourceEdit** This section lists all the sources in a table. All the elements can be edited and filtered.
### Source edition - Parent component
`src/js/components/Dashboard/Sources/SourceEdit.js`

**SourceEdit** Connects NormalSourceEdit.js and ExpandSourceEdit.js to editSources and subsources
### Analysis Board 
`src/js/components/Dashboard/SystemStatus/AnalysisBoard.js`

**AnalysisBoard** Each users analysis board. Featuring their data and their charts
### Charts - Rewatched Videos
`src/js/components/Dashboard/SystemStatus/Charts/RewatchedVideos.js`

**RewatchedVideos** Display the Charts of the using the recharts package
### Charts - Seen Videos
`src/js/components/Dashboard/SystemStatus/Charts/SeenVideos.js`

**SeenVideos** Display the Charts of the using the recharts package
### Charts - Unseen Videos
`src/js/components/Dashboard/SystemStatus/Charts/UnseenVideos.js`

**UnseenVideos** Display the Charts of the using the recharts package

### Server state
`src/js/components/Dashboard/SystemStatus/Servers.js`

**Servers** Provides an interface to see the state of the available services in Jenkins and server status
### Usage Analytics
`src/js/components/Dashboard/SystemStatus/UsageAnalysis.js`

**UsageAnalysis** Provides an interface to filter charts according to users, date, type of video injection and user interactions
### User add
`src/js/components/Dashboard/Users/Add/UserAdd.js`

**UserAdd** Component to add users. The form is a child component
### User Edit 
`src/js/components/Dashboard/Users/UserEdit.js`

**UserEdit** Component to edit users. All the data is displayed on table where the items can be edited.
### Check Videos
`src/js/components/Dashboard/Videos/VideosCheck.js`

**VideosCheck** Component to all the created videos
### Editable DropDown
`src/js/components/EditableField/EditableDropdown.js`7

**EditableDropdown** A dropdown that can get text inputs

Property | Type | Required | Description
:--- | :--- | :--- | :---
name|string|yes|The input name

### Editable Field
`src/js/components/EditableField/EditableField.js`

**EditableField** An editable text field input

Property | Type | Required | Description
:--- | :--- | :--- | :---
name|string|yes|The input name

### Render Field 
`src/js/components/Form/Validator.js`

**RenderField** Input Form Field with Error and Warnings Outputs according to validations
### Render Dropdown List 
**RenderDropdownList** Dropdown Form Field with Error and Warnings Outputs according to validations
### Header
`src/js/components/Header.js`

**Header** Header with logo and loged in user. In this case the user is hard coded
### Login
`src/js/components/Login/Login.js`

**Login** This components is for the Dashboard Login page and connects to a FormLogin component
### Page Indexer
`src/js/components/PageIndexer/PageIndexer.js`

**PageIndexer** Indexes pages according to the number of elements of a list from a json object

Property | Type | Required | Description
:--- | :--- | :--- | :---
pageNumber|number|yes|Number of pages to divide the content to

### Calendar Filter
`src/js/components/SearchFilter/CalendarFilter.js`

**CalendarFilter** A calendar to filter content according to a date range

Property | Type | Required | Description
:--- | :--- | :--- | :---
filter|||

### Search Filter
`src/js/components/SearchFilter/SearchFilter.js`

**SearchFilter** A text based search to filter content according to a text query

Property | Type | Required | Description
:--- | :--- | :--- | :---
filter|||
